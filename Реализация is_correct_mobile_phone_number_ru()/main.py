def check_brackets(string: str):
    counter = 0
    for char in string:
        if char == "(":
            counter += 1
        elif char == ")":
            counter -= 1
        if counter < 0:
            return False
    return not counter


def check_number(num: str):
    clear = clear_number(num)
    return not (not (num.startswith("+7") or num.startswith("8")) or
                num.startswith("-") or
                num.endswith("-") or
                "--" in number or
                not check_brackets(num) or
                num.count("(") > 1 or
                len(clear) != 11 or
                not clear.isnumeric())


def clear_number(num: str):
    return "".join(filter(lambda char: char not in ("+", "-", " ", "(", ")", "\t"), num))


def process_number(num: str):
    return "+7" + clear_number(num)[-10:]


number = input().strip()
if check_number(number):
    print("YES")
else:
    print("NO")