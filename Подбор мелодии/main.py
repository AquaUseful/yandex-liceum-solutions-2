N = 7
PITCHES = ("до", "ре", "ми", "фа", "соль", "ля", "си")
LONG_PITCHES = ("до-о", "ре-э", "ми-и", "фа-а", "со-оль", "ля-а", "си-и")
INTERVALS = ("прима", "секунда", "терция", "кварта",
             "квинта", "секста", "септима")


class Note:
    def __init__(self, tone, long=False):
        self._tone = PITCHES.index(tone)
        self._long = long

    def get_interval(self, other):
        return INTERVALS[abs(self._tone - other._tone)]

    def __str__(self):
        return LONG_PITCHES[self._tone] if self._long else PITCHES[self._tone]

    def __rshift__(self, other):
        if self._tone + other > 6:
            raise IndexError
        return Note(PITCHES[self._tone + other], self._long)

    def __lshift__(self, other):
        if self._tone - other < 0:
            raise IndexError
        return Note(PITCHES[(self._tone - other) % N], self._long)

    def __eq__(self, other):
        return self._tone == other._tone

    def __lt__(self, other):
        return self._tone < other._tone

    def __gt__(self, other):
        return self._tone > other._tone

    def __ne__(self, other):
        return self._tone != other._tone

    def __le__(self, other):
        return self._tone <= other._tone

    def __ge__(self, other):
        return self._tone >= other._tone


class Melody:
    def __init__(self, notes=[]):
        self._notes = list(notes)

    def __str__(self):
        return ", ".join(map(str, self._notes)).capitalize()

    def append(self, note):
        self._notes.append(note)

    def replace_last(self, note):
        self._notes[-1] = note

    def remove_last(self):
        del self._notes[-1]

    def clear(self):
        self._notes = []

    def __len__(self):
        return len(self._notes)

    def __rshift__(self, other):
        try:
            return Melody(map(lambda note: note >> other, self._notes))
        except IndexError:
            return Melody(self._notes)

    def __lshift__(self, other):
        try:
            return Melody(map(lambda note: note << other, self._notes))
        except IndexError:
            return Melody(self._notes)
