import pytest
from yandex_testing_lesson import is_under_queen_attack


def test_typeerr():
    with pytest.raises(TypeError):
        is_under_queen_attack(123, "a1")
    with pytest.raises(TypeError):
        is_under_queen_attack(345.456, "b5")
    with pytest.raises(TypeError):
        is_under_queen_attack([5, "45"], "a1")
    with pytest.raises(TypeError):
        is_under_queen_attack({}, "c5")
    with pytest.raises(TypeError):
        is_under_queen_attack([], "a6")
    with pytest.raises(TypeError):
        is_under_queen_attack("b5", 123)
    with pytest.raises(TypeError):
        is_under_queen_attack("b5", 345.456)
    with pytest.raises(TypeError):
        is_under_queen_attack("a1", [5, "45"])
    with pytest.raises(TypeError):
        is_under_queen_attack("a1", {})
    with pytest.raises(TypeError):
        is_under_queen_attack("a1", [])


def test_valerr():
    with pytest.raises(ValueError):
        is_under_queen_attack("grw", "d5")
    with pytest.raises(ValueError):
        is_under_queen_attack("44", "c3")
    with pytest.raises(ValueError):
        is_under_queen_attack("", "d5")
    with pytest.raises(ValueError):
        is_under_queen_attack("p", "d5")
    with pytest.raises(ValueError):
        is_under_queen_attack("d5", "grw")
    with pytest.raises(ValueError):
        is_under_queen_attack("c3", "44")
    with pytest.raises(ValueError):
        is_under_queen_attack("d5", "")
    with pytest.raises(ValueError):
        is_under_queen_attack("d5", "p")


def test_normal():
    assert is_under_queen_attack("h8", "a1")
    assert is_under_queen_attack("a8", "a1")
    assert is_under_queen_attack("h1", "a1")
    assert is_under_queen_attack("a5", "e5")
    assert is_under_queen_attack("a1", "a1")
    assert not is_under_queen_attack("f4", "h8")
    assert not is_under_queen_attack("c4", "b2")
