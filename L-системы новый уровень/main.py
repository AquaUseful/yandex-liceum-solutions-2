from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QColorDialog, QFileDialog, QInputDialog, QMessageBox
from PyQt5.QtGui import QPixmap
from PIL import Image, ImageQt
from PIL.ImageDraw import Draw
from math import sin, cos, pi
from itertools import chain


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.bg_color = "#000000"
        self.line_color = "#FFFFFF"
        self.image = None
        self.clear_path()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.spinBox_step.valueChanged.connect(self.update_step_slider)
        self.horizontalSlider_step.valueChanged.connect(
            self.update_step_spinbox)
        self.pushButton_update.clicked.connect(self.update_label)
        self.pushButton_ch_bg_color.clicked.connect(self.request_bg_color)
        self.pushButton_ch_line_color.clicked.connect(self.request_line_color)
        self.pushButton_clear.clicked.connect(self.clear_label)
        self.radioButton_angle_mode.clicked.connect(self.set_angle_mode)
        self.radioButton_div_mode.clicked.connect(self.set_plane_div_mode)
        self.pushButton_save_image.clicked.connect(self.save_image)
        self.action_open.triggered.connect(self.open)
        self.action_new.triggered.connect(self.new)
        self.action_save.triggered.connect(self.save)
        self.action_saveas.triggered.connect(self.saveas)
        self.action_exit.triggered.connect(exit)

    def new(self):
        self.clear_path()
        self.spinBox_angle.setValue(0)
        self.spinBox_plane_div.setValue(2)
        self.spinBox_start_angle.setValue(0)
        self.spinBox_step.setValue(1)
        self.horizontalSlider_step.setValue(1)
        self.lineEdit_name.clear()
        self.lineEdit_initiator.clear()
        self.plainTextEdit_rules.clear()
        self.clear_label()

    def save_data(self):
        name = self.lineEdit_name.text()
        if not name:
            name = "lsystem"
        if self.radioButton_angle_mode.isChecked():
            angle_div = self.spinBox_angle.value()
        else:
            angle_div = self.spinBox_plane_div.value()
        initiator = self.lineEdit_initiator.text()
        rules_text = self.plainTextEdit_rules.toPlainText()
        with open(self.path, "w") as f:
            f.write(name + "\n")
            f.write(str(angle_div) + "\n")
            f.write(initiator + "\n")
            f.write(rules_text + "\n")

    def save(self):
        if self.path is not None:
            self.save_data()
        else:
            self.saveas()

    def saveas(self):
        try:
            self.path = self.requset_save_path(
                "Сохранить l-систему", "l-system files (*.ls)")
            self.save_data()
        except FileNotFoundError:
            return

    def open(self):
        try:
            self.path = self.request_open_path(
                "Открыть l-систему", "l-system files (*.ls)")
        except FileNotFoundError:
            return
        self.load_data()

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()

    def load_data(self):
        with open(self.path, "r") as f:
            text = f.read()
        lines = text.strip().split("\n")
        lines = tuple(line.strip() for line in lines)
        print(lines)
        print(*map(lambda line: " " in line, lines[3:]))
        if not (lines[1].isdecimal() and
                all(map(lambda line: " " in line, lines[3:]))):
            self.clear_path()
            self.show_messagebox("Неверный формат файла!")
            return
        try:
            mode = self.request_choice(
                "Как загрузить данные", "Выберите режим поворотов", ("Деление плоскости", "Угол"))
        except InterruptedError:
            self.clear_path()
            return
        name = lines[0]
        angle_div = int(lines[1])
        initiator = lines[2]
        rules_text = "\n".join(lines[3:])
        if mode == "Угол":
            self.set_angle_mode()
            self.spinBox_angle.setValue(angle_div)
        else:
            self.set_plane_div_mode()
            self.spinBox_plane_div.setValue(angle_div)
        self.lineEdit_name.setText(name)
        self.lineEdit_initiator.setText(initiator)
        self.plainTextEdit_rules.setPlainText(rules_text)

    def clear_path(self):
        self.path = None

    def request_choice(self, header, request, variants):
        val, ok = QInputDialog.getItem(
            self, header, request, variants, editable=False)
        if ok:
            return val
        else:
            raise InterruptedError

    def request_bg_color(self):
        try:
            self.bg_color = self.request_color()
        except ValueError:
            return

    def request_line_color(self):
        try:
            self.line_color = self.request_color()
        except ValueError:
            return

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def requset_save_path(self, request, filter):
        path = QFileDialog.getSaveFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def request_color(self):
        color = QColorDialog.getColor()
        if color.isValid():
            return color.name()
        else:
            raise ValueError

    def request_bg_color(self):
        try:
            self.bg_color = self.request_color()
        except ValueError:
            return

    def request_line_color(self):
        try:
            self.line_color = self.request_color()
        except ValueError:
            return

    def generate_image(self, scale=1):
        if not self.check_values():
            raise ValueError
        size = (self.spinBox_size_x.value() * scale,
                self.spinBox_size_y.value() * scale)
        start_coords = (self.spinBox_start_x.value() * scale,
                        self.spinBox_start_y.value() * scale)
        start_coords = decart_to_image_coords(start_coords, size)
        if self.radioButton_div_mode.isChecked():
            rotation_angle = angle_part_of_circle(
                self.spinBox_plane_div.value())
        else:
            rotation_angle = deg_to_rad(self.spinBox_angle.value())
        start_angle = deg_to_rad(self.spinBox_start_angle.value())
        rules = strings_to_dict(self.get_rule_strings(), " ")
        step_length = self.spinBox_step_length.value() * scale
        initiator = self.lineEdit_initiator.text()
        iterations = self.horizontalSlider_step.value()
        string_gen_params = (initiator, rules, iterations)
        draw_params = (start_coords, start_angle, rotation_angle,
                       step_length, self.line_color)
        image_params = (size, self.bg_color, draw_params)
        self.image = gen_string_and_draw(string_gen_params, image_params)

    def check_values(self):
        return self.lineEdit_initiator.text() and self.plainTextEdit_rules.toPlainText()

    def get_rule_strings(self):
        text = self.plainTextEdit_rules.toPlainText()
        return text.split("\n")

    def update_step_slider(self):
        self.horizontalSlider_step.setValue(self.spinBox_step.value())

    def update_step_spinbox(self):
        self.spinBox_step.setValue(self.horizontalSlider_step.value())

    def update_label(self):
        try:
            self.generate_image()
        except ValueError:
            return
        pixmap = image_to_pixmap(self.image)
        self.label_result.setPixmap(pixmap)

    def clear_label(self):
        self.label_result.clear()

    def set_angle_mode(self):
        self.radioButton_angle_mode.setChecked(True)
        self.spinBox_angle.setEnabled(True)
        self.spinBox_plane_div.setEnabled(False)

    def set_plane_div_mode(self):
        self.radioButton_div_mode.setChecked(True)
        self.spinBox_angle.setEnabled(False)
        self.spinBox_plane_div.setEnabled(True)

    def save_image(self):
        try:
            path = self.requset_save_path("Сохранить", "All files")
        except FileNotFoundError:
            return
        scale = self.spinBox_scale.value()
        self.generate_image(scale)
        img = self.image.convert("RGB")
        img.save(path)


def image_to_pixmap(image):
    return QPixmap.fromImage(ImageQt.ImageQt(image))


def deg_to_rad(angle):
    return angle * pi / 180


def angle_part_of_circle(part):
    return 2 * pi / part


def decart_to_image_coords(coords, image_size):
    return (image_size[0] // 2 + coords[0], image_size[1] // 2 - coords[1])


def strings_to_dict(strings, separator):
    return {string.split(separator)[0]: string.split(separator)[1] for string in strings}


def point_on_circle(center_coords, radius, angle):
    return (round(center_coords[0] + radius * sin(angle)),
            round(center_coords[1] + radius * cos(angle)))


def generate_lsystem_string(initiator, rules, iterations):
    string = initiator
    rules = {rule[0]: rule[1] for rule in rules.items()}
    for _ in range(iterations):
        string = chain(
            *map(lambda char: rules[char] if char in rules else char, string))
    return string


def draw_lsystem(draw, lsystem, start_coords, start_angle, rotation_angle, step_length, line_color, max_coords):
    not_drawn_lines = 0
    x, y = start_coords[0], start_coords[1]
    angle = start_angle
    stack = []
    for char in lsystem:
        if char == "F":
            new_x, new_y = point_on_circle((x, y), step_length, angle)
            if check_coords((x, y), (new_x, new_y), max_coords):
                draw.line(((x, y), (new_x, new_y)), fill=line_color, width=1)
                not_drawn_lines = 0
            else:
                not_drawn_lines += 1
            x, y = new_x, new_y
        elif char == "f":
            x, y = point_on_circle((x, y), step_length, angle)
        elif char == "+":
            angle += rotation_angle
        elif char == "-":
            angle -= rotation_angle
        elif char == "|":
            angle += pi
        elif char == "[":
            stack.append((x, y, angle))
        elif char == "]":
            x, y, angle = stack.pop()
        if not_drawn_lines >= 30000:
            break


def check_coords(coords, new_coords, max_coords):
    return (0 <= coords[0] <= max_coords[0] and
            0 <= coords[1] <= max_coords[1]) or \
        (0 <= new_coords[0] <= max_coords[0] and
         0 <= new_coords[1] <= max_coords[1])


def gen_lsystem_image(lsystem, size, bg_color, draw_params):
    img = Image.new("RGBA", size, bg_color)
    dr = Draw(img)
    draw_lsystem(dr, lsystem, *draw_params, size)
    del dr
    return img


def gen_string_and_draw(string_gen_params, image_params):
    print("Start string generation")
    lsystem = generate_lsystem_string(*string_gen_params)
    print("String generated, start drawing")
    img = gen_lsystem_image(lsystem, *image_params)
    print("Drawing finished")
    return img


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
