import pygame
from itertools import cycle, islice


def main():
    size, count = map(int, input().split())
    pygame.init()
    screen = pygame.display.set_mode((size * count * 2, ) * 2)
    colors = cycle((pygame.Color("red"), pygame.Color(
        "green"), pygame.Color("blue")))
    color_seq = iter(tuple(islice(colors, count))[::-1])
    center = size * count
    for num in range(count, 0, -1):
        pygame.draw.circle(screen, next(color_seq), (center, ) * 2, size * num)
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
