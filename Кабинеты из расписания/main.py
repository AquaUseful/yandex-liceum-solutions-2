from sys import stdin
rooms = dict()
for line in stdin:
    words = line.split()
    if not (words and words[-1].isdigit()):
        continue
    room_num = int(words[-1])
    subj = " ".join(words[:-1])
    if room_num in rooms:
        if subj not in rooms[room_num]:
            rooms[room_num].append(subj)
    else:
        rooms[room_num] = [subj]
print(*map(lambda item: f"{item[0]}: {', '.join(item[1])}",
           sorted(rooms.items(), key=lambda val: val[0])), sep='\n')
