import requests
import argparse
import sys
from PIL import Image
import io
import math


def lonlat_distance(a, b):
    degree_to_meters_factor = 111 * 1000
    a_lon, a_lat = a
    b_lon, b_lat = b
    radians_lattitude = math.radians((a_lat + b_lat) / 2.)
    lat_lon_factor = math.cos(radians_lattitude)
    dx = abs(a_lon - b_lon) * degree_to_meters_factor * lat_lon_factor
    dy = abs(a_lat - b_lat) * degree_to_meters_factor
    distance = math.sqrt(dx * dx + dy * dy)
    return distance


def request_get(server_address, params):
    response = requests.get(server_address, params)
    if not response:
        print(params)
        print("HTTP status:", response.status_code, "(", response.reason, ")")
        sys.exit(1)
    return response


parser = argparse.ArgumentParser()
parser.add_argument("address", type=str)
args = parser.parse_args()

server_address = "http://geocode-maps.yandex.ru/1.x/"
api_key = "40d1649f-0493-4b70-98ba-98533de7710b"
params = {
    "apikey": api_key,
    "geocode": args.address,
    "format": "json"}

response = request_get(server_address, params)
json_resp = response.json()
coords = json_resp["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"]
float_coords = tuple(map(float, coords.split()))
coords = coords.replace(" ", ",")
server_address = "https://search-maps.yandex.ru/v1/"
api_key = "dda3ddba-c9ea-4ead-9010-f43fbc15c6e3"
search_text = "аптека"
params = {
    "apikey": api_key,
    "text": search_text,
    "lang": "ru_RU",
    "ll": coords,
    "format": "json"}
response = request_get(server_address, params)
json_resp = response.json()
org = json_resp["features"][0]
org_coords = org["geometry"]["coordinates"]
org_name = org["properties"]["CompanyMetaData"]["name"]
org_address = org["properties"]["CompanyMetaData"]["address"]
org_worktime = org["properties"]["CompanyMetaData"]["Hours"]["text"]
distance = lonlat_distance(float_coords, org_coords)

server_address = "http://static-maps.yandex.ru/1.x/"
params = {
    "l": "map",
    "pt": f"{coords},comma~{','.join(map(str, org_coords))},flag"
}
response = request_get(server_address, params)
Image.open(io.BytesIO(response.content)).show()
print("Адрес аптеки:", org_address)
print("Название аптеки:", org_name)
print("Время работы аптеки:", org_worktime)
print("Расстояние до аптеки:", round(distance, 2), "м")
print(org_coords)