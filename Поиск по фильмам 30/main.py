from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QFileDialog, QMessageBox
import sqlite3


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.action_open.triggered.connect(self.load_database)
        self.action_exit.triggered.connect(exit)
        self.db = None

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.update_table)
        self.pushButton_2.clicked.connect(self.clear_table)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.get_col_names()

    def fill_table(self, header, data):
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(str(el))
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def clear_table(self):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def execute_query_fetchone(self, query):
        return self.cur.execute(query).fetchone()

    def close_cursor(self):
        self.cur.close()

    def get_col_names(self):
        self.create_cursor()
        self.execute_query_fetchone("SELECT * FROM Films")
        names = map(lambda val: val[0], self.cur.description)
        self.close_cursor()
        return tuple(names)

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()

    @staticmethod
    def gen_sql_query(conditions, table_name, cols):
        return f"""SELECT {', '.join(cols)} FROM {table_name}
        WHERE {' AND '.join(conditions)}"""

    def update_table(self):
        if self.db is None:
            self.show_messagebox("Сначала откройте БД!")
            return
        title_mode = self.buttonGroup_title.checkedButton().text()
        year_mode = self.buttonGroup_year.checkedButton().text()
        duration_mode = self.buttonGroup_duration.checkedButton().text()
        conditions = []
        for col, mode, text_field in zip(("title", "year", "duration"),
                                         (title_mode, year_mode, duration_mode),
                                         (self.plainTextEdit_title, self.plainTextEdit_year, self.plainTextEdit_duration)):
            if not text_field.toPlainText():
                continue
            if mode == "SQL":
                conditions.append(col + " " + text_field.toPlainText())
            else:
                conditions.append(f"{col} = '{text_field.toPlainText()}'")
        header = self.get_col_names()
        query = self.gen_sql_query(conditions, "Films", "*")
        print(header)
        print(query)
        self.create_cursor()
        try:
            data = self.execute_query(query)
        except sqlite3.OperationalError:
            self.show_messagebox("Неверный фильтр!")
            return
        self.fill_table(header, data)
        self.close_cursor()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
