from sys import argv, exit
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem, QFileDialog, QMessageBox
import sqlite3


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(676, 729)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_4.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(
            self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.plainTextEdit_title = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.plainTextEdit_title.setObjectName("plainTextEdit_title")
        self.verticalLayout.addWidget(self.plainTextEdit_title)
        self.radioButton = QtWidgets.QRadioButton(self.groupBox_2)
        self.radioButton.setChecked(True)
        self.radioButton.setObjectName("radioButton")
        self.buttonGroup_title = QtWidgets.QButtonGroup(MainWindow)
        self.buttonGroup_title.setObjectName("buttonGroup_title")
        self.buttonGroup_title.addButton(self.radioButton)
        self.verticalLayout.addWidget(self.radioButton)
        self.radioButton_2 = QtWidgets.QRadioButton(self.groupBox_2)
        self.radioButton_2.setObjectName("radioButton_2")
        self.buttonGroup_title.addButton(self.radioButton_2)
        self.verticalLayout.addWidget(self.radioButton_2)
        self.horizontalLayout.addWidget(self.groupBox_2)
        self.groupBox_3 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.plainTextEdit_year = QtWidgets.QPlainTextEdit(self.groupBox_3)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.plainTextEdit_year.sizePolicy().hasHeightForWidth())
        self.plainTextEdit_year.setSizePolicy(sizePolicy)
        self.plainTextEdit_year.setObjectName("plainTextEdit_year")
        self.verticalLayout_2.addWidget(self.plainTextEdit_year)
        self.radioButton_3 = QtWidgets.QRadioButton(self.groupBox_3)
        self.radioButton_3.setChecked(True)
        self.radioButton_3.setObjectName("radioButton_3")
        self.buttonGroup_year = QtWidgets.QButtonGroup(MainWindow)
        self.buttonGroup_year.setObjectName("buttonGroup_year")
        self.buttonGroup_year.addButton(self.radioButton_3)
        self.verticalLayout_2.addWidget(self.radioButton_3)
        self.radioButton_4 = QtWidgets.QRadioButton(self.groupBox_3)
        self.radioButton_4.setObjectName("radioButton_4")
        self.buttonGroup_year.addButton(self.radioButton_4)
        self.verticalLayout_2.addWidget(self.radioButton_4)
        self.horizontalLayout.addWidget(self.groupBox_3)
        self.groupBox_4 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_4.setObjectName("groupBox_4")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox_4)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.plainTextEdit_duration = QtWidgets.QPlainTextEdit(self.groupBox_4)
        self.plainTextEdit_duration.setObjectName("plainTextEdit_duration")
        self.verticalLayout_3.addWidget(self.plainTextEdit_duration)
        self.radioButton_5 = QtWidgets.QRadioButton(self.groupBox_4)
        self.radioButton_5.setChecked(True)
        self.radioButton_5.setObjectName("radioButton_5")
        self.buttonGroup_duration = QtWidgets.QButtonGroup(MainWindow)
        self.buttonGroup_duration.setObjectName("buttonGroup_duration")
        self.buttonGroup_duration.addButton(self.radioButton_5)
        self.verticalLayout_3.addWidget(self.radioButton_5)
        self.radioButton_6 = QtWidgets.QRadioButton(self.groupBox_4)
        self.radioButton_6.setObjectName("radioButton_6")
        self.buttonGroup_duration.addButton(self.radioButton_6)
        self.verticalLayout_3.addWidget(self.radioButton_6)
        self.horizontalLayout.addWidget(self.groupBox_4)
        self.verticalLayout_4.addWidget(self.groupBox)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_2.addWidget(self.pushButton_2)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(2)
        sizePolicy.setHeightForWidth(
            self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout_4.addWidget(self.tableWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 676, 21))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.action_open = QtWidgets.QAction(MainWindow)
        self.action_open.setObjectName("action_open")
        self.action_exit = QtWidgets.QAction(MainWindow)
        self.action_exit.setObjectName("action_exit")
        self.menu.addAction(self.action_open)
        self.menu.addSeparator()
        self.menu.addAction(self.action_exit)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "Выбор фильтров"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Название"))
        self.radioButton.setText(_translate("MainWindow", "Обычный"))
        self.radioButton_2.setText(_translate("MainWindow", "SQL"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Год выпуска"))
        self.radioButton_3.setText(_translate("MainWindow", "Обычный"))
        self.radioButton_4.setText(_translate("MainWindow", "SQL"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Длительность"))
        self.radioButton_5.setText(_translate("MainWindow", "Обычный"))
        self.radioButton_6.setText(_translate("MainWindow", "SQL"))
        self.pushButton.setText(_translate("MainWindow", "Отфильтровать"))
        self.pushButton_2.setText(_translate("MainWindow", "Очистить таблицу"))
        self.menu.setTitle(_translate("MainWindow", "Файл"))
        self.action_open.setText(_translate("MainWindow", "Открыть БД..."))
        self.action_exit.setText(_translate("MainWindow", "Выход"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.action_open.triggered.connect(self.load_database)
        self.action_exit.triggered.connect(exit)
        self.db = None

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.pushButton.clicked.connect(self.update_table)
        self.pushButton_2.clicked.connect(self.clear_table)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.get_col_names()

    def fill_table(self, header, data):
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(str(el))
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def clear_table(self):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def execute_query_fetchone(self, query):
        return self.cur.execute(query).fetchone()

    def close_cursor(self):
        self.cur.close()

    def get_col_names(self):
        self.create_cursor()
        self.execute_query_fetchone("SELECT * FROM Films")
        names = map(lambda val: val[0], self.cur.description)
        self.close_cursor()
        return tuple(names)

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()

    @staticmethod
    def gen_sql_query(conditions, table_name, cols):
        return f"""SELECT {', '.join(cols)} FROM {table_name}
        WHERE {' AND '.join(conditions)}"""

    def update_table(self):
        if self.db is None:
            self.show_messagebox("Сначала откройте БД!")
            return
        title_mode = self.buttonGroup_title.checkedButton().text()
        year_mode = self.buttonGroup_year.checkedButton().text()
        duration_mode = self.buttonGroup_duration.checkedButton().text()
        conditions = []
        for col, mode, text_field in zip(("title", "year", "duration"),
                                         (title_mode, year_mode, duration_mode),
                                         (self.plainTextEdit_title, self.plainTextEdit_year, self.plainTextEdit_duration)):
            if not text_field.toPlainText():
                continue
            if mode == "SQL":
                conditions.append(col + " " + text_field.toPlainText())
            else:
                conditions.append(f"{col} = '{text_field.toPlainText()}'")
        header = self.get_col_names()
        query = self.gen_sql_query(conditions, "Films", "*")
        print(header)
        print(query)
        self.create_cursor()
        try:
            data = self.execute_query(query)
        except sqlite3.OperationalError:
            self.show_messagebox("Неверный фильтр!")
            return
        self.fill_table(header, data)
        self.close_cursor()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
