from sys import stdin
from re import split
string = stdin.read().lower().replace("\n", " ")
sentencies = {}
for sign, sentence in zip(tuple(filter(lambda el: el in ".!?", string)),
                          split("[!?.]", string)):
    if sign not in sentencies:
        sentencies[sign] = set(sentence.split())
        continue
    sentencies[sign] |= set(sentence.split())
print(*sorted(tuple((sentencies["."] & sentencies["?"]) - sentencies["!"])))
