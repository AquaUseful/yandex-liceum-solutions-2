from random import randrange
with open("lines.txt", "r", encoding="utf-8") as file:
    rline = randrange(0, sum(1 for _ in file))
    file.seek(0)
    for num, line in enumerate(file):
        if num == rline:
            print(line.strip())
            break
