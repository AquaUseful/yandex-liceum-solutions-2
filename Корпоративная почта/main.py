string = " ".join(input() for _ in range(int(input())))
for _ in range(int(input())):
    name = input()
    if name in string:
        print(f"{name}{string.count(name)}@untitled.py")
    else:
        print(f"{name}@untitled.py")
    string += (" " + name)
