from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QInputDialog
from PyQt5.QtGui import QPixmap
from PIL import Image, ImageQt
from PIL.ImageDraw import Draw
from random import randint


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(294, 371)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Генерация случайного флага"))
        self.groupBox.setTitle(_translate("MainWindow", "Результат"))
        self.label.setText(_translate("MainWindow", "TextLabel"))
        self.pushButton.setText(_translate("MainWindow", "Генерировать флаг"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.pushButton.clicked.connect(self.update_label)

    def make_flag(self):
        lines, ok = QInputDialog.getInt(
            self, "Введите количество цветов", "Количество: ", 2, 2, 300)
        if not ok:
            return
        flag = Image.new("RGBA", (200, 300))
        dr = Draw(flag)
        line_heigth = 300 // lines
        for line in range(lines):
            color = (randint(0, 255), randint(0, 255), randint(0, 255))
            dr.rectangle(((0, line_heigth * line),
                          (300, line_heigth * (line + 1))), fill=color, outline=color)
        del dr
        return flag

    @staticmethod
    def image_to_pixmap(image):
        return QPixmap.fromImage(ImageQt.ImageQt(image))

    def update_label(self):
        pixmap = self.image_to_pixmap(self.make_flag())
        self.label.setPixmap(pixmap)


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
