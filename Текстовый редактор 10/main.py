from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.actionSaveFile.triggered.connect(self.save_file)
        self.actionOpenFile.triggered.connect(self.load_file)
        self.actionExit.triggered.connect(self.app_exit)

    def load_file(self):
        with open("edit.txt", "r") as f:
            data = f.read()
        self.textBrowser.setText(data)

    def save_file(self):
        with open("edit.txt", "w") as f:
            f.write(self.textBrowser.toPlainText())

    def app_exit(self):
        exit()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
