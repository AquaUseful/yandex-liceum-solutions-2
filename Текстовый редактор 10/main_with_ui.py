from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(598, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser.setReadOnly(False)
        self.textBrowser.setObjectName("textBrowser")
        self.gridLayout.addWidget(self.textBrowser, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 598, 21))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.actionSaveFile = QtWidgets.QAction(MainWindow)
        self.actionSaveFile.setObjectName("actionSaveFile")
        self.actionOpenFile = QtWidgets.QAction(MainWindow)
        self.actionOpenFile.setObjectName("actionOpenFile")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setShortcutContext(
            QtCore.Qt.WidgetWithChildrenShortcut)
        self.actionExit.setObjectName("actionExit")
        self.menu.addAction(self.actionSaveFile)
        self.menu.addAction(self.actionOpenFile)
        self.menu.addSeparator()
        self.menu.addAction(self.actionExit)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Текстровый редактор 1.0"))
        self.menu.setTitle(_translate("MainWindow", "Файл"))
        self.actionSaveFile.setText(_translate("MainWindow", "Сохранить"))
        self.actionOpenFile.setText(_translate("MainWindow", "Открыть"))
        self.actionExit.setText(_translate("MainWindow", "Выход"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.actionSaveFile.triggered.connect(self.save_file)
        self.actionOpenFile.triggered.connect(self.load_file)
        self.actionExit.triggered.connect(self.app_exit)

    def load_file(self):
        with open("edit.txt", "r") as f:
            data = f.read()
        self.textBrowser.setText(data)

    def save_file(self):
        with open("edit.txt", "w") as f:
            f.write(self.textBrowser.toPlainText())

    def app_exit(self):
        exit()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
