from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QListWidgetItem


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.add_contact)

    def add_contact(self):
        item = QListWidgetItem()
        item.setText(f"{self.lineEdit.text()}\t{self.lineEdit_2.text()}")
        self.listWidget.addItem(item)
        self.lineEdit.setText("")
        self.lineEdit_2.setText("")


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
