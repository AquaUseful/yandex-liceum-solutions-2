class Bell:
    def __init__(self, *args, **kwargs):
        self._info = args
        self._named_info = sorted(kwargs.items(), key=lambda val: val[0])

    def print_info(self):
        from itertools import starmap
        if not self._info and not self._named_info:
            print("-")
        else:
            print(", ".join(starmap(lambda name, par: f"{name}: {par}",
                                    self._named_info)), ", ".join(self._info),
                  sep=('; ' if self._info and self._named_info else ''))


class LittleBell(Bell):
    def sound(self):
        print("ding")


class BigBell(Bell):
    def __init__(self, *args, **kwargs):
        self._dong = False
        super().__init__(*args, **kwargs)

    def sound(self):
        print("dong" if self._dong else "ding")
        self._dong = not self._dong
