from sys import stdin


def check_word(src, word):
    src, word = list(src), list(word)
    while src and word:
        if word[-1] not in src:
            break
        src.remove(word.pop())
    return not word


src_word = tuple(input())
corr_words = tuple(filter(lambda el: check_word(src_word, el),
                          map(lambda st: st.strip('\n'), stdin.readlines())))
print(len(corr_words))
print(*corr_words, sep='\n')
