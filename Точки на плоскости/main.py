points = tuple(map(lambda itr: tuple(map(int, itr)),
                   (input().split() for _ in range(int(input())))))
on_axis = tuple(filter(lambda el: 0 in el, points))
if on_axis:
    print(*on_axis, sep='\n')
print(*map(lambda el: el[0] + " " + str(len(tuple(filter(lambda point: el[1](*point), points)))),
           (("I:", lambda x, y: x > 0 and y > 0),
            ("II:", lambda x, y: x < 0 and y > 0),
            ("III:", lambda x, y: x < 0 and y < 0),
            ("IV:", lambda x, y: x > 0 and y < 0))), sep=', ', end='')
print(".")
