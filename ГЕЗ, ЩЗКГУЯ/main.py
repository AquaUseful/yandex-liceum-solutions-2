def list_shift(lst, shift):
    length = len(lst)
    return lst[shift % length:] + lst[:shift % length]


alp = input()
shift = int(input())

print(list_shift(alp, shift))
print(alp)
print(list_shift(alp, -shift))
