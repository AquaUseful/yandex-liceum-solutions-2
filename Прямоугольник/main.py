import pygame


def main():
    width, heigth = map(int, input().split())
    pygame.init()
    screen = pygame.display.set_mode((width, heigth))
    color = pygame.Color("red")
    rectangle = pygame.Rect(1, 1, width - 2, heigth - 2)
    pygame.draw.rect(screen, color, rectangle)
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
