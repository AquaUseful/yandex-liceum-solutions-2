import sys

sort = False
vals = []
for arg in sys.argv[1:]:
    if arg == "--sort":
        sort = True
    else:
        vals.append(arg.split("="))
if sort:
    vals.sort(key=lambda val: val[0])
print("\n".join(map(lambda val: f"Key: {val[0]} Value: {val[1]}", vals)))
