from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QFileDialog
from PyQt5.QtGui import QPixmap
from PyQt5 import QtCore, QtGui, QtWidgets
from PIL import Image, ImageQt


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 504)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setText("")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.buttonLoadImage = QtWidgets.QPushButton(self.centralwidget)
        self.buttonLoadImage.setObjectName("buttonLoadImage")
        self.verticalLayout.addWidget(self.buttonLoadImage)
        self.checkBoxRed = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxRed.setObjectName("checkBoxRed")
        self.verticalLayout.addWidget(self.checkBoxRed)
        self.checkBoxGreen = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxGreen.setObjectName("checkBoxGreen")
        self.verticalLayout.addWidget(self.checkBoxGreen)
        self.checkBoxBlue = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxBlue.setObjectName("checkBoxBlue")
        self.verticalLayout.addWidget(self.checkBoxBlue)
        self.buttonTurnLeft = QtWidgets.QPushButton(self.centralwidget)
        self.buttonTurnLeft.setObjectName("buttonTurnLeft")
        self.verticalLayout.addWidget(self.buttonTurnLeft)
        self.buttonTurnRight = QtWidgets.QPushButton(self.centralwidget)
        self.buttonTurnRight.setObjectName("buttonTurnRight")
        self.verticalLayout.addWidget(self.buttonTurnRight)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        spacerItem = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PIL 2.0"))
        self.buttonLoadImage.setText(_translate(
            "MainWindow", "Загрузить картинку"))
        self.checkBoxRed.setText(_translate("MainWindow", "Красный канал"))
        self.checkBoxGreen.setText(_translate("MainWindow", "Зелёный канал"))
        self.checkBoxBlue.setText(_translate("MainWindow", "Синий канал"))
        self.buttonTurnLeft.setText(
            _translate("MainWindow", "Повернуть влево"))
        self.buttonTurnRight.setText(
            _translate("MainWindow", "Повернуть вправо"))
        self.pushButton.setText(_translate("MainWindow", "Save Image"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.image = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
        self.update_label()

    def initUI(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.buttonLoadImage.clicked.connect(self.load_image)
        self.buttonTurnLeft.clicked.connect(self.rotate_left)
        self.buttonTurnRight.clicked.connect(self.rotate_right)
        self.pushButton.clicked.connect(self.save_image)
        self.checkBoxRed.clicked.connect(self.update_label)
        self.checkBoxGreen.clicked.connect(self.update_label)
        self.checkBoxBlue.clicked.connect(self.update_label)

    def update_image_channels(self):
        bands = []
        if self.checkBoxRed.isChecked():
            bands.append(self.image.getchannel("R"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        if self.checkBoxGreen.isChecked():
            bands.append(self.image.getchannel("G"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        if self.checkBoxBlue.isChecked():
            bands.append(self.image.getchannel("B"))
        else:
            bands.append(Image.new("L", self.image.size, 0))
        self.res_image = Image.merge("RGB", bands)

    def update_label(self):
        self.update_image_channels()
        self.pixmap = QPixmap.fromImage(ImageQt.ImageQt(self.res_image))
        self.label.setPixmap(self.pixmap)

    def rotate_left(self):
        self.image = self.image.transpose(Image.ROTATE_90)
        self.update_label()

    def rotate_right(self):
        self.image = self.image.transpose(Image.ROTATE_270)
        self.update_label()

    def load_image(self):
        fname = QFileDialog.getOpenFileName(self, "Загрузить картинку", "", "Images (*.png *.xpm *.jpg *.jpeg)")[0]
        if fname:
            self.image = Image.open(fname)
            self.image = self.image.convert("RGB")
            self.update_label()

    def save_image(self):
        self.image.save("res.jpg")


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
