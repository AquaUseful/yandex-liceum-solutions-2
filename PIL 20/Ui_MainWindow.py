# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/catsatan/Документы/yandex_2/PIL 20/MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 504)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setText("")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.buttonLoadImage = QtWidgets.QPushButton(self.centralwidget)
        self.buttonLoadImage.setObjectName("buttonLoadImage")
        self.verticalLayout.addWidget(self.buttonLoadImage)
        self.checkBoxRed = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxRed.setObjectName("checkBoxRed")
        self.verticalLayout.addWidget(self.checkBoxRed)
        self.checkBoxGreen = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxGreen.setObjectName("checkBoxGreen")
        self.verticalLayout.addWidget(self.checkBoxGreen)
        self.checkBoxBlue = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxBlue.setObjectName("checkBoxBlue")
        self.verticalLayout.addWidget(self.checkBoxBlue)
        self.buttonTurnLeft = QtWidgets.QPushButton(self.centralwidget)
        self.buttonTurnLeft.setObjectName("buttonTurnLeft")
        self.verticalLayout.addWidget(self.buttonTurnLeft)
        self.buttonTurnRight = QtWidgets.QPushButton(self.centralwidget)
        self.buttonTurnRight.setObjectName("buttonTurnRight")
        self.verticalLayout.addWidget(self.buttonTurnRight)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PIL 2.0"))
        self.buttonLoadImage.setText(_translate("MainWindow", "Загрузить картинку"))
        self.checkBoxRed.setText(_translate("MainWindow", "Красный канал"))
        self.checkBoxGreen.setText(_translate("MainWindow", "Зелёный канал"))
        self.checkBoxBlue.setText(_translate("MainWindow", "Синий канал"))
        self.buttonTurnLeft.setText(_translate("MainWindow", "Повернуть влево"))
        self.buttonTurnRight.setText(_translate("MainWindow", "Повернуть вправо"))
        self.pushButton.setText(_translate("MainWindow", "Save Image"))
