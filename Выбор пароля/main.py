import sys

key_seq_en = "qwertyuiop;asdfghjkl;zxcvbnm"
key_seq_rus = "йцукенгшщзхъ;фывапролджэё;ячсмитьбю"
key_seq_num = "1234567890"


class PasswordError(Exception):
    pass


class LengthError(PasswordError):
    pass


class LetterError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class SequenceError(PasswordError):
    pass


def check_length(passwd: str):
    if len(passwd) < 9:
        raise LengthError()


def check_letters(passwd: str):
    upper = False
    lower = False
    for char in passwd:
        if char.isalpha():
            upper = upper or char.isupper()
            lower = lower or char.islower()
        if upper and lower:
            return
    raise LetterError()


def check_digit(passwd: str):
    if not any(map(lambda char: char.isdigit(), passwd)):
        raise DigitError()


def check_seq(passwd: str):
    passwd = passwd.lower()
    while len(passwd) >= 3:
        triplet = passwd[:3]
        passwd = passwd[1:]
        if triplet in key_seq_en or \
                triplet in key_seq_rus or \
                triplet in key_seq_num:
            raise SequenceError()


def check_password(passwd: str):
    try:
        check_length(passwd)
        check_letters(passwd)
        check_digit(passwd)
        check_seq(passwd)
        return "ok"
    except PasswordError as exc:
        return exc.__class__.__name__


result = ""
try:
    for line in sys.stdin:
        line = line.strip()
        if line == "Ctrl+Break":
            raise KeyboardInterrupt
        result = check_password(line)
        if result == "ok":
            raise KeyboardInterrupt
        else:
            print(result)
except KeyboardInterrupt:
    if result == "ok":
        print("ok")
    else:
        print("Bye-Bye")
