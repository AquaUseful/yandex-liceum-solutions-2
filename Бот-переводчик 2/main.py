import asyncio
import urllib
import requests
import collections
from discord.ext import commands

TOKEN = ""
TRANSLATOR_APIKEY = ""


class Translator(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.lang = collections.defaultdict(lambda: "en-ru")

    @commands.command(name="help_bot")
    async def help_bot(self, ctx):
        await ctx.send("Help for translator bot")
        await ctx.send("!!help_bot - show this help")
        await ctx.send("!!set_lang <lang1>-<lang2> - set translator to thanslate from lang1 to lang2")
        await ctx.send("!!text <text> - translate text")

    @commands.command(name="set_lang")
    async def set_lang(self, ctx, lang):
        self.lang[ctx.channel] = lang
        await ctx.send("Translator set to lang: " + lang)

    @commands.command(name="text")
    async def text(self, ctx, *args):
        server_address = "https://translate.yandex.net/api/v1.5/tr.json/translate"
        text = " ".join(args)
        url = f"{server_address}?key={TRANSLATOR_APIKEY}&lang={self.lang[ctx.channel]}"
        data = {"text": text}
        resp = requests.post(url, data)
        if not resp:
            await ctx.send("Something went wrong!")
            await ctx.send(f"Error {resp.status_code} ({resp.reason})")
        resp_json = resp.json()
        trans_text = urllib.parse.unquote(resp_json["text"][0])
        await ctx.send(trans_text)


bot = commands.Bot(command_prefix="!!")
bot.add_cog(Translator(bot))
bot.run(TOKEN)
