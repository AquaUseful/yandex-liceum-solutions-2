from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
import random

WAITING, WAITING_SUPHLER = range(2)
POEM = """Ласточки пропали,
А вчера зарёй
Всё грачи летали
Да, как сеть, мелькали
Вон над той горой.
С вечера все спится,
На дворе темно.
Лист сухой валится,
Ночью ветер злится
Да стучит в окно.
Лучше б снег да вьюгу
Встретить грудью рад!
Словно как с испугу
Раскричавшись, к югу
Журавли летят.
Выйдешь - поневоле
Тяжело - хоть плачь!
Смотришь - через поле
Перекати-поле
Прыгает, как мяч."""

TOKEN = ""
REQUEST_KWARGS = {"proxy_url": "socks5h://127.0.0.1:9050"}


def start(update, context):
    context.chat_data["poem_stack"] = POEM.split("\n")[::-1]
    update.message.reply_text(context.chat_data["poem_stack"].pop(),
                              reply_markup=ReplyKeyboardRemove())
    return WAITING


def stop(update, context):
    update.message.reply_text("Работа завершена",
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def error(update, context):
    update.message.reply_text(f"Update {update} caused {context.error}")


def check_poem_end(update, context):
    return not context.chat_data["poem_stack"]


def wait_str(update, context):
    msg_text = update.message.text
    if msg_text == context.chat_data["poem_stack"][-1]:
        context.chat_data["poem_stack"].pop()
        try:
            update.message.reply_text(context.chat_data["poem_stack"].pop())
            if not context.chat_data["poem_stack"]:
                raise IndexError()
        except IndexError:
            update.message.reply_text(
                "Ура!\nНапшите '/start', чтобы повторить")
            return stop(update, context)
        return WAITING
    else:
        keyboard = [["/suphler"]]
        update.message.reply_text("Нет, не так",
                                  reply_markup=ReplyKeyboardMarkup(keyboard,
                                                                   resize_keyboard=True,
                                                                   one_time_keyboard=True))
        return WAITING_SUPHLER


def suphler(update, context):
    update.message.reply_text("Шепчет на ухо: " + context.chat_data["poem_stack"][-1],
                              reply_markup=ReplyKeyboardRemove())
    return WAITING


def main():
    updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS, use_context=True)
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start, pass_chat_data=True)],
        states={
            WAITING: [MessageHandler(Filters.text, wait_str, pass_chat_data=True)],
            WAITING_SUPHLER: [MessageHandler(Filters.text, wait_str, pass_chat_data=True),
                              CommandHandler("suphler", suphler, pass_chat_data=True)]
        },
        fallbacks=[CommandHandler("stop", stop)]
    )

    dp.add_handler(conv_handler)
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
