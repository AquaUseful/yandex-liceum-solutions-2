import shutil
import os
import datetime


def make_reserve_arc(source, dest):
    if not os.path.exists(source):
        raise FileNotFoundError(f"'{source}' not found")
    if not os.path.isdir(source):
        raise TypeError(f"'{source}' is a file, directory excepted")
    if not os.path.exists(dest):
        raise FileNotFoundError(f"'{dest}' not found")
    if not os.path.isdir(dest):
        raise TypeError(f"'{dest}' is a file, directory excepted")
    curr_datetime = datetime.datetime.now()
    str_datetime = curr_datetime.strftime("%Y%m%d_%H%M%S")
    archive_name = f"archive_{source.split('/')[-1]}_{str_datetime}"
    shutil.make_archive(archive_name, "zip", source)
    shutil.move(archive_name + ".zip", dest)
