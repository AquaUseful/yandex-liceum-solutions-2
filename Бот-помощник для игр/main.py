from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
import random

MAIN_MENU, DICE_MENU, TIMER_MENU, TIMER_SET = range(4)

TOKEN = ""
REQUEST_KWARGS = {"proxy_url": "socks5h://127.0.0.1:9050"}


def start(update, context):
    keyboard = [["/dice", "/timer"]]
    update.message.reply_text("Select what you want to do",
                              reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True))
    return MAIN_MENU


def stop(update, context):
    update.message.reply_text("Stopped",
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def dice_menu(update, context):
    keyboard = [["1. 6 faces", "2. 2 x 6 faces"],
                ["3. 20 faces", "4. back"]]
    update.message.reply_text("Select dice type",
                              reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True))
    return DICE_MENU


def dice(update, context):
    cmd = update.message.text.split(". ")[0]
    if cmd == "1":
        msg = "Result: " + str(random.randint(1, 6))
    elif cmd == "2":
        msg = "Result: " + str(random.randint(1, 6)) + " " + \
            str(random.randint(1, 6))
    elif cmd == "3":
        msg = "Result: " + str(random.randint(1, 20))
    elif cmd == "4":
        return start(update, context)
    update.message.reply_text(msg)


def timer_menu(update, context):
    keyboard = [["1. 30 seconds", "2. 1 min"],
                ["3. 5 min", "4. back"]]
    update.message.reply_text("Select timer",
                              reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True))
    return TIMER_MENU


def timer(update, context):
    cmd = update.message.text.split(". ")[0]
    if cmd == "1":
        due = 30
    elif cmd == "2":
        due = 60
    elif cmd == "3":
        due = 5 * 60
    elif cmd == "4":
        return start(update, context)
    chat_id = update.message.chat_id
    if "job" in context.chat_data:
        old_job = context.chat_data["job"]
        old_job.schedule_removal()
    new_job = context.job_queue.run_once(
        timer_alarm, due, context={"chat_id": chat_id, "due": due})
    context.chat_data["job"] = new_job
    keyboard = [["/close"]]
    update.message.reply_text(f"Timer set for: {due} seconds",
                              reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True))
    return TIMER_SET


def cancel_timer(update, context):
    if "job" not in context.chat_data:
        update.message.reply_text("No timer set")
    job = context.chat_data["job"]
    job.schedule_removal()
    del context.chat_data["job"]
    update.message.reply_text("Timer cancelled")
    return timer_menu(update, context)


def timer_alarm(context):
    job = context.job
    context.bot.send_message(
        job.context["chat_id"], f"{job.context['due']} seconds passed!")


def error(update, context):
    update.message.reply_text(f"Update {update} caused {context.error}")


def main():
    updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS, use_context=True)
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            MAIN_MENU: [CommandHandler("dice", dice_menu), CommandHandler("timer", timer_menu)],
            DICE_MENU: [MessageHandler(Filters.regex("^([1-4]\. (.*))$"), dice)],
            TIMER_MENU: [MessageHandler(Filters.regex("^([1-4]\. (.*))$"), timer, pass_chat_data=True, pass_job_queue=True)],
            TIMER_SET: [CommandHandler(
                "close", cancel_timer, pass_chat_data=True)]
        },
        fallbacks=[CommandHandler("stop", stop)]
    )

    dp.add_handler(conv_handler)
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
