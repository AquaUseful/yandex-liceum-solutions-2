from math import sqrt
x1, y1, radius1 = map(int, input().split())
x2, y2, radius2 = map(int, input().split())
print("YES" if sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2) <= radius1 + radius2 else "NO")
