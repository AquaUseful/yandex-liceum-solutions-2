from PIL import Image, ImageChops


def crop_opject(image, bg_color):
    bg = Image.new("RGB", image.size, bg_color)
    bbox = ImageChops.difference(image, bg).getbbox()
    return image.crop(bbox)


img = Image.open("image.png")
res = crop_opject(img, img.getpixel((0, 0)))
res.save("res.png")