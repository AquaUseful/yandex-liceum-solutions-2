import pygame


class Board(object):
    def __init__(self, width: int, heigth: int, fillval: int = 0) -> None:
        self.width = width
        self.heigth = heigth
        self.board = [[fillval] * heigth for _ in range(width)]
        self.set_view(10, 10, 30)

    def set_view(self, left: int, top: int, cell_size: int) -> None:
        self.left = left
        self.top = top
        self.cell_size = cell_size


    def render(self) -> None:
        color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.heigth):
                width = {0: 1, 1: 0}[self.board[x][y]]
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.rect(screen, color, rect, width)



running = True
fps = 100
screen_heigth, screen_width = 500, 500

board = Board(5, 6)

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_heigth))
clock = pygame.time.Clock()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill(pygame.Color("black"))
    board.render()
    pygame.display.flip()
    clock.tick(fps)
pygame.quit()
