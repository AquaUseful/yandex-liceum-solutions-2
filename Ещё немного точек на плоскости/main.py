points = tuple(map(lambda itr: tuple(map(int, itr)),
                   (input().split() for _ in range(int(input())))))
in_range = tuple(filter(lambda coords: coords[0] > coords[1] and coords[0] > -coords[1] or
                        -coords[0] > coords[1] and -coords[0] > -coords[1], points))
if in_range:
    print(*in_range, sep='\n')
print(*map(lambda el: f"{el[0]} {max(points, key=el[1])}",
           zip(("left:", "right:", "top:", "bottom:"),
               (lambda val: -val[0], lambda val: val[0], lambda val: val[1], lambda val: -val[1]))),
      sep='\n')
