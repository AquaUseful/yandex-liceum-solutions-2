def check_brackets(string: str):
    counter = 0
    for char in string:
        if char == "(":
            counter += 1
        elif char == ")":
            counter -= 1
    assert not counter


def check_brackets_count(string: str):
    assert string.count("(") <= 1


def check_length(string):
    assert len(string) == 11


def check_country_code(num: str):
    assert num.startswith("+7") or num.startswith("8")


def check_numeric(num: str):
    assert num.isnumeric()


def check_hyphens(num: str):
    assert not (num.startswith("-") or num.endswith("-") or "--" in number)


def check_number(num: str):
    check_country_code(num)
    check_hyphens(num)
    check_brackets(num)
    check_brackets_count(num)
    clear = clear_number(num)
    check_length(clear)
    check_numeric(clear)


def clear_number(num: str):
    return "".join(filter(lambda char: char not in ("+", "-", " ", "(", ")", "\t"), num))


def process_number(num: str):
    return "+7" + clear_number(num)[-10:]


number = input().strip()
try:
    check_number(number)
    print(process_number(number))
except Exception:
    print("error")
