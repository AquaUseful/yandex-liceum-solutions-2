from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QMessageBox
from PyQt5.QtGui import QFont


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(268, 344)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setContentsMargins(10, 10, 10, 10)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2.addLayout(self.gridLayout, 0, 1, 1, 2)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_2.addWidget(self.pushButton)
        self.gridLayout_2.addLayout(self.verticalLayout_2, 1, 2, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.radioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton.setChecked(True)
        self.radioButton.setObjectName("radioButton")
        self.buttonGroup = QtWidgets.QButtonGroup(MainWindow)
        self.buttonGroup.setObjectName("buttonGroup")
        self.buttonGroup.addButton(self.radioButton)
        self.verticalLayout.addWidget(self.radioButton)
        self.radioButton_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_2.setObjectName("radioButton_2")
        self.buttonGroup.addButton(self.radioButton_2)
        self.verticalLayout.addWidget(self.radioButton_2)
        self.gridLayout_2.addLayout(self.verticalLayout, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Крестики-Нолики"))
        self.label.setText(_translate("MainWindow", "TextLabel"))
        self.pushButton.setText(_translate("MainWindow", "Начать заново"))
        self.radioButton.setText(_translate("MainWindow", "Начинают нолики"))
        self.radioButton_2.setText(_translate(
            "MainWindow", "Начинают крестики"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.field = [["", "", ""] for _ in range(3)]
        self.setupUi(self)
        self.retranslateUi(self)
        self.initUI()
        self.connect_buttons()
        self.ch_start_player()
        self.show_player()

    def initUI(self):
        self.buttons = [[] for _ in range(3)]
        font = QFont()
        font.setPointSize(20)
        sizePolicy = (QtWidgets.QSizePolicy.Minimum,
                      QtWidgets.QSizePolicy.Minimum)
        for x in range(3):
            for y in range(3):
                button = QPushButton(parent=self)
                button.setFont(font)
                button.setSizePolicy(*sizePolicy)
                button.clicked.connect(self.make_move)
                self.gridLayout.addWidget(button, x, y)
                self.buttons[x].append(button)
        self.set_enabled_radio_buttons(True)
        self.pushButton.setEnabled(True)

    def connect_buttons(self):
        self.buttonGroup.buttonClicked.connect(self.ch_start_player)
        self.pushButton.clicked.connect(self.restart_game)

    def restart_game(self):
        self.pushButton.setEnabled(False)
        del self.buttons
        del self.field
        self.field = [["", "", ""] for _ in range(3)]
        self.ch_start_player()
        self.initUI()

    def make_move(self):
        button = self.sender()
        for x in enumerate(self.buttons):
            if button in x[1]:
                x, y = x[0], x[1].index(button)
                break
        print(x, y)
        button.setText(self.player)
        button.clicked.disconnect()
        self.set_enabled_radio_buttons(False)
        self.field[x][y] = self.player
        self.ch_player()
        self.show_player()
        win = self.check_win()
        print(win)
        if win == "-" and "" in sum(self.field, []):
            return
        else:
            message = QMessageBox(self)
            if win in ("X", "0"):
                message.setText(f"Выграл игрок: {win}")
            else:
                message.setText(f"Ничья")
            message.show()
            self.restart_game()

    def set_enabled_radio_buttons(self, state):
        self.radioButton.setEnabled(state)
        self.radioButton_2.setEnabled(state)

    def ch_player(self):
        self.player = {"0": "X", "X": "0"}[self.player]

    def ch_start_player(self):
        if self.buttonGroup.checkedButton() == self.radioButton:
            self.player = "0"
        else:
            self.player = "X"
        print(self.player)
        self.show_player()

    def show_player(self):
        self.label.setText(f"Сейчас ходит: {self.player}")

    def check_win(self):
        for field in (self.field, transpon(self.field)):
            print(*self.field, sep='\n')
            for line in field:
                if 'XXX' in ''.join(line):
                    return 'X'
                elif '000' in ''.join(line):
                    return '0'
            if field[0][0] == field[1][1] == field[2][2] and field[1][1] in ("X", "0"):
                print("here")
                return field[0][0]
            elif field[0][2] == field[1][1] == field[2][0] and field[1][1] in ("X", "0"):
                print("here2")
                return field[0][2]
        return "-"


def transpon(matrix):
    return [[matrix[x][y] for x in range(len(matrix))] for y in range(len(matrix[0]))]


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
