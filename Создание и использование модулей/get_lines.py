import argparse


def count_lines(path: str):
    try:
        with open(path) as f:
            lines = f.readlines()
        return len(lines)
    except Exception:
        return 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", required=True, type=str)
    args = parser.parse_args()
    print(count_lines(args.file))


if __name__ == "__main__":
    main()
