with open("prices.txt", "r") as inp:
    print(sum(map(lambda line:
                  int(line.split("\t")[1]) * float(line.split("\t")[2]), inp)))
