import pytest
from yandex_testing_lesson import count_chars


def test_count_chars_empty():
    assert count_chars("") == {}


def test_count_chars_one_char():
    assert count_chars("1") == {"1": 1}
    assert count_chars("a") == {"a": 1}


def test_count_chars_same_char():
    assert count_chars("aaaaaa") == {"a": 6}
    assert count_chars("6666666666") == {"6": 10}


def test_count_chars_normal():
    assert count_chars("aabccabcc") == {"a": 3, "b": 2, "c": 4}
    assert count_chars("556jh55hhgg6hh6ee6y8g") == {'5': 4,
                                                    '6': 4,
                                                    'j': 1,
                                                    'h': 5,
                                                    'g': 3,
                                                    'e': 2,
                                                    'y': 1,
                                                    '8': 1}

def test_count_chars_nostring():
    with pytest.raises(TypeError):
        count_chars(None)
    with pytest.raises(TypeError):
        count_chars(1237)
    with pytest.raises(TypeError):
        count_chars(2.7)
    with pytest.raises(TypeError):
        count_chars({})
    with pytest.raises(TypeError):
        count_chars([])
    with pytest.raises(TypeError):
        count_chars(int)

