from csv import DictReader
school, grade = map(int, input().split())
matched = []
with open("rez.csv") as csv_results:
    reader = DictReader(csv_results, delimiter=',', quotechar='"')
    for result in reader:
        login_data = result["login"].split("-")
        if int(login_data[2]) == school and int(login_data[3]) == grade:
            matched.append(result)
matched = map(lambda result: (result["user_name"].split()[
              3], int(result["Score"])), matched)
matched = sorted(matched, key=lambda val: (val[1], val[0]), reverse=True)
print(*map(lambda el: f"{el[0]} {el[1]}", matched), sep='\n')
