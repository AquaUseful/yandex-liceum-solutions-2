import pygame

running = True
v = 20
pygame.init()
screen = pygame.display.set_mode((500, 500))
clock = pygame.time.Clock()
grow = False
while running:
    screen.fill(pygame.Color("blue"))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONUP:
            grow = True
            radius = 0
            circle_pos = event.pos
    if grow:
        pygame.draw.circle(screen, pygame.Color(
            "yellow"), circle_pos, int(radius))
        radius += v * clock.tick() / 1000
    pygame.display.flip()
