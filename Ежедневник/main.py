from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow, QListWidgetItem
from PyQt5 import uic


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.add_event)
        self.pushButton_2.clicked.connect(self.remove_selected)

    def add_event(self):
        text = self.plainTextEdit.toPlainText()
        if not text:
            return
        date = self.calendarWidget.selectedDate().toString("dd.MM.yy")
        time = self.timeEdit.time().toString("hh:mm")
        self.plainTextEdit.setPlainText("")
        item = QListWidgetItem()
        item.setText(f"{date} {time}\t{text}")
        self.listWidget.addItem(item)

    def remove_selected(self):
        for item in self.listWidget.selectedItems():
            self.listWidget.takeItem(self.listWidget.row(item))


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
