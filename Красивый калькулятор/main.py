import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from math import sqrt, factorial


class Calculator(QMainWindow):
    def __init__(self):
        self._expression = ""
        super().__init__()
        self._initUI()

    def _initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.buttBackspace.clicked.connect(self._backspace)
        self.buttDel.clicked.connect(self._clear_expr)
        self.buttNum_0.clicked.connect(self._char_butt_click)
        self.buttNum_000.clicked.connect(self._char_butt_click)
        self.buttNum_1.clicked.connect(self._char_butt_click)
        self.buttNum_2.clicked.connect(self._char_butt_click)
        self.buttNum_3.clicked.connect(self._char_butt_click)
        self.buttNum_4.clicked.connect(self._char_butt_click)
        self.buttNum_5.clicked.connect(self._char_butt_click)
        self.buttNum_6.clicked.connect(self._char_butt_click)
        self.buttNum_7.clicked.connect(self._char_butt_click)
        self.buttNum_8.clicked.connect(self._char_butt_click)
        self.buttNum_9.clicked.connect(self._char_butt_click)
        self.buttDot.clicked.connect(self._char_butt_click)
        self.buttMinus.clicked.connect(self._char_butt_click)
        self.buttPlus.clicked.connect(self._char_butt_click)
        self.buttMultiply.clicked.connect(self._char_butt_click)
        self.buttDivide.clicked.connect(self._char_butt_click)
        self.buttOpBr.clicked.connect(self._char_butt_click)
        self.buttClBr.clicked.connect(self._char_butt_click)
        self.buttPow.clicked.connect(self._char_butt_click)
        self.buttFactorial.clicked.connect(self._factorial_result_click)
        self.buttSqrt.clicked.connect(self._sqrt_butt_click)
        self.buttResult.clicked.connect(self._result_butt_click)

    # This method is for buttons which only add its text to expression
    def _add_to_expr(self, char):
        self._expression += char
        self._update_labels()

    def _backspace(self):
        self._expression = self._expression[:-1]
        self._update_labels()

    def _clear_expr(self):
        self._expression = ""
        self._update_labels()

    def _char_butt_click(self):
        self._add_to_expr(self.sender().text())

    def _update_labels(self):
        self.labelExpression.setText(self._expression)
        self.labelResult.setText(str(self._calculate_expr()))

    def _sqrt_butt_click(self):
        self._add_to_expr("sqrt(")

    def _factorial_result_click(self):
        self._add_to_expr("fact(")

    def _calculate_expr(self):
        op_dict = {"^": "**",
                   "fact(": "factorial("}
        evaluable = self._expression
        for op in op_dict.items():
            evaluable = evaluable.replace(op[0], op[1])
        print(evaluable)
        try:
            return eval(evaluable)
        except:
            return "ОШИБКА"

    def _result_butt_click(self):
        if self._calculate_expr() != "ОШИБКА":
            self._expression = str(self._calculate_expr())
            self._update_labels()


def main():
    app = QApplication(sys.argv)
    window = Calculator()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
