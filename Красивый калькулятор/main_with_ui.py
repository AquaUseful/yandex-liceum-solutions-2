import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from math import sqrt, factorial
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(696, 316)
        MainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        MainWindow.setAnimated(True)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setContentsMargins(12, 12, 12, 12)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.labelExpression = QtWidgets.QLabel(self.centralwidget)
        self.labelExpression.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.labelExpression.sizePolicy().hasHeightForWidth())
        self.labelExpression.setSizePolicy(sizePolicy)
        self.labelExpression.setMinimumSize(QtCore.QSize(0, 50))
        self.labelExpression.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.labelExpression.setFrameShadow(QtWidgets.QFrame.Plain)
        self.labelExpression.setTextFormat(QtCore.Qt.AutoText)
        self.labelExpression.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.labelExpression.setWordWrap(False)
        self.labelExpression.setTextInteractionFlags(
            QtCore.Qt.NoTextInteraction)
        self.labelExpression.setObjectName("labelExpression")
        self.verticalLayout.addWidget(self.labelExpression)
        self.labelResult = QtWidgets.QLabel(self.centralwidget)
        self.labelResult.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.labelResult.sizePolicy().hasHeightForWidth())
        self.labelResult.setSizePolicy(sizePolicy)
        self.labelResult.setMinimumSize(QtCore.QSize(0, 50))
        self.labelResult.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.labelResult.setFrameShadow(QtWidgets.QFrame.Plain)
        self.labelResult.setTextFormat(QtCore.Qt.AutoText)
        self.labelResult.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.labelResult.setWordWrap(False)
        self.labelResult.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.labelResult.setObjectName("labelResult")
        self.verticalLayout.addWidget(self.labelResult)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.charButtons = QtWidgets.QGridLayout()
        self.charButtons.setObjectName("charButtons")
        self.buttNum_7 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_7.setObjectName("buttNum_7")
        self.charButtons.addWidget(self.buttNum_7, 0, 0, 1, 1)
        self.buttNum_8 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_8.setObjectName("buttNum_8")
        self.charButtons.addWidget(self.buttNum_8, 0, 1, 1, 1)
        self.buttNum_9 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_9.setObjectName("buttNum_9")
        self.charButtons.addWidget(self.buttNum_9, 0, 2, 1, 1)
        self.buttNum_4 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_4.setObjectName("buttNum_4")
        self.charButtons.addWidget(self.buttNum_4, 1, 0, 1, 1)
        self.buttNum_5 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_5.setObjectName("buttNum_5")
        self.charButtons.addWidget(self.buttNum_5, 1, 1, 1, 1)
        self.buttNum_6 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_6.setObjectName("buttNum_6")
        self.charButtons.addWidget(self.buttNum_6, 1, 2, 1, 1)
        self.buttNum_1 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_1.setObjectName("buttNum_1")
        self.charButtons.addWidget(self.buttNum_1, 2, 0, 1, 1)
        self.buttNum_2 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_2.setObjectName("buttNum_2")
        self.charButtons.addWidget(self.buttNum_2, 2, 1, 1, 1)
        self.buttNum_3 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_3.setObjectName("buttNum_3")
        self.charButtons.addWidget(self.buttNum_3, 2, 2, 1, 1)
        self.buttNum_0 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_0.setObjectName("buttNum_0")
        self.charButtons.addWidget(self.buttNum_0, 3, 0, 1, 1)
        self.buttDot = QtWidgets.QPushButton(self.centralwidget)
        self.buttDot.setObjectName("buttDot")
        self.charButtons.addWidget(self.buttDot, 3, 1, 1, 1)
        self.buttNum_000 = QtWidgets.QPushButton(self.centralwidget)
        self.buttNum_000.setObjectName("buttNum_000")
        self.charButtons.addWidget(self.buttNum_000, 3, 2, 1, 1)
        self.horizontalLayout.addLayout(self.charButtons)
        spacerItem = QtWidgets.QSpacerItem(
            42, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.opButtons = QtWidgets.QGridLayout()
        self.opButtons.setObjectName("opButtons")
        self.buttDivide = QtWidgets.QPushButton(self.centralwidget)
        self.buttDivide.setObjectName("buttDivide")
        self.opButtons.addWidget(self.buttDivide, 3, 1, 1, 1)
        self.buttMinus = QtWidgets.QPushButton(self.centralwidget)
        self.buttMinus.setObjectName("buttMinus")
        self.opButtons.addWidget(self.buttMinus, 1, 1, 1, 1)
        self.buttSqrt = QtWidgets.QPushButton(self.centralwidget)
        self.buttSqrt.setObjectName("buttSqrt")
        self.opButtons.addWidget(self.buttSqrt, 1, 0, 1, 1)
        self.buttPow = QtWidgets.QPushButton(self.centralwidget)
        self.buttPow.setObjectName("buttPow")
        self.opButtons.addWidget(self.buttPow, 2, 0, 1, 1)
        self.buttPlus = QtWidgets.QPushButton(self.centralwidget)
        self.buttPlus.setObjectName("buttPlus")
        self.opButtons.addWidget(self.buttPlus, 0, 1, 1, 1)
        self.buttMultiply = QtWidgets.QPushButton(self.centralwidget)
        self.buttMultiply.setObjectName("buttMultiply")
        self.opButtons.addWidget(self.buttMultiply, 2, 1, 1, 1)
        self.buttFactorial = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.buttFactorial.sizePolicy().hasHeightForWidth())
        self.buttFactorial.setSizePolicy(sizePolicy)
        self.buttFactorial.setObjectName("buttFactorial")
        self.opButtons.addWidget(self.buttFactorial, 0, 0, 1, 1)
        self.buttResult = QtWidgets.QPushButton(self.centralwidget)
        self.buttResult.setObjectName("buttResult")
        self.opButtons.addWidget(self.buttResult, 4, 2, 1, 1)
        self.buttClBr = QtWidgets.QPushButton(self.centralwidget)
        self.buttClBr.setObjectName("buttClBr")
        self.opButtons.addWidget(self.buttClBr, 4, 1, 1, 1)
        self.buttOpBr = QtWidgets.QPushButton(self.centralwidget)
        self.buttOpBr.setObjectName("buttOpBr")
        self.opButtons.addWidget(self.buttOpBr, 4, 0, 1, 1)
        self.buttBackspace = QtWidgets.QPushButton(self.centralwidget)
        self.buttBackspace.setObjectName("buttBackspace")
        self.opButtons.addWidget(self.buttBackspace, 0, 2, 1, 1)
        self.buttDel = QtWidgets.QPushButton(self.centralwidget)
        self.buttDel.setObjectName("buttDel")
        self.opButtons.addWidget(self.buttDel, 1, 2, 1, 1)
        self.horizontalLayout.addLayout(self.opButtons)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.buttNum_7.setText(_translate("MainWindow", "7"))
        self.buttNum_8.setText(_translate("MainWindow", "8"))
        self.buttNum_9.setText(_translate("MainWindow", "9"))
        self.buttNum_4.setText(_translate("MainWindow", "4"))
        self.buttNum_5.setText(_translate("MainWindow", "5"))
        self.buttNum_6.setText(_translate("MainWindow", "6"))
        self.buttNum_1.setText(_translate("MainWindow", "1"))
        self.buttNum_2.setText(_translate("MainWindow", "2"))
        self.buttNum_3.setText(_translate("MainWindow", "3"))
        self.buttNum_0.setText(_translate("MainWindow", "0"))
        self.buttDot.setText(_translate("MainWindow", "."))
        self.buttNum_000.setText(_translate("MainWindow", "000"))
        self.buttDivide.setText(_translate("MainWindow", "/"))
        self.buttMinus.setText(_translate("MainWindow", "-"))
        self.buttSqrt.setText(_translate("MainWindow", "sqrt"))
        self.buttPow.setText(_translate("MainWindow", "^"))
        self.buttPlus.setText(_translate("MainWindow", "+"))
        self.buttMultiply.setText(_translate("MainWindow", "*"))
        self.buttFactorial.setText(_translate("MainWindow", "!"))
        self.buttResult.setText(_translate("MainWindow", "="))
        self.buttClBr.setText(_translate("MainWindow", ")"))
        self.buttOpBr.setText(_translate("MainWindow", "("))
        self.buttBackspace.setText(_translate("MainWindow", "<<"))
        self.buttDel.setText(_translate("MainWindow", "C"))


class Calculator(QMainWindow, Ui_MainWindow):
    def __init__(self):
        self._expression = ""
        super().__init__()
        self._initUI()

    def _initUI(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.buttBackspace.clicked.connect(self._backspace)
        self.buttDel.clicked.connect(self._clear_expr)
        self.buttNum_0.clicked.connect(self._char_butt_click)
        self.buttNum_000.clicked.connect(self._char_butt_click)
        self.buttNum_1.clicked.connect(self._char_butt_click)
        self.buttNum_2.clicked.connect(self._char_butt_click)
        self.buttNum_3.clicked.connect(self._char_butt_click)
        self.buttNum_4.clicked.connect(self._char_butt_click)
        self.buttNum_5.clicked.connect(self._char_butt_click)
        self.buttNum_6.clicked.connect(self._char_butt_click)
        self.buttNum_7.clicked.connect(self._char_butt_click)
        self.buttNum_8.clicked.connect(self._char_butt_click)
        self.buttNum_9.clicked.connect(self._char_butt_click)
        self.buttDot.clicked.connect(self._char_butt_click)
        self.buttMinus.clicked.connect(self._char_butt_click)
        self.buttPlus.clicked.connect(self._char_butt_click)
        self.buttMultiply.clicked.connect(self._char_butt_click)
        self.buttDivide.clicked.connect(self._char_butt_click)
        self.buttOpBr.clicked.connect(self._char_butt_click)
        self.buttClBr.clicked.connect(self._char_butt_click)
        self.buttPow.clicked.connect(self._char_butt_click)
        self.buttFactorial.clicked.connect(self._factorial_result_click)
        self.buttSqrt.clicked.connect(self._sqrt_butt_click)
        self.buttResult.clicked.connect(self._result_butt_click)

    # This method is for buttons which only add its text to expression
    def _add_to_expr(self, char):
        self._expression += char
        self._update_labels()

    def _backspace(self):
        self._expression = self._expression[:-1]
        self._update_labels()

    def _clear_expr(self):
        self._expression = ""
        self._update_labels()

    def _char_butt_click(self):
        self._add_to_expr(self.sender().text())

    def _update_labels(self):
        self.labelExpression.setText(self._expression)
        self.labelResult.setText(str(self._calculate_expr()))

    def _sqrt_butt_click(self):
        self._add_to_expr("sqrt(")

    def _factorial_result_click(self):
        self._add_to_expr("fact(")

    def _calculate_expr(self):
        op_dict = {"^": "**",
                   "fact(": "factorial("}
        evaluable = self._expression
        for op in op_dict.items():
            evaluable = evaluable.replace(op[0], op[1])
        print(evaluable)
        try:
            return eval(evaluable)
        except:
            return "ОШИБКА"

    def _result_butt_click(self):
        if self._calculate_expr() != "ОШИБКА":
            self._expression = str(self._calculate_expr())
            self._update_labels()


def main():
    app = QApplication(sys.argv)
    window = Calculator()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
