import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
import random
import datetime

TOKEN = "7f07c1e868331a9bafe689b78c0bf3fd426f0668f9e118586f5318f2b059eeaca1de0ed7ad9095c99e369"
GROUP_ID = 193176805


def main():
    vk_session = vk_api.VkApi(token=TOKEN)

    longpoll = VkBotLongPoll(vk_session, GROUP_ID)
    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            msg_text = event.obj.message["text"]
            usr_id = event.obj.message["from_id"]
            vk = vk_session.get_api()
            user = vk.users.get(user_ids=str(usr_id), fields="city")[0]
            msg = f"Привет, {user['first_name']}!"
            if user["city"]:
                msg += f"\nКак поживает {user['city']['title']}?"
            vk.messages.send(user_id=usr_id, message=msg,
                             random_id=random.randint(0, 2 ** 64))


if __name__ == "__main__":
    main()
