import zipfile
import json

counter = 0
with zipfile.ZipFile("input.zip") as archive:
    for path in archive.namelist():
        if path.endswith(".json"):
            with archive.open(path, "r") as jsonfile:
                data = json.loads(jsonfile.read())
            if data["city"] == "Москва":
                counter += 1
print(counter)
