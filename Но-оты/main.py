class Note:
    def __init__(self, tone, long=False):
        if long:
            self._tone = {"до": "до-о", "ре": "ре-э",
                          "ми": "ми-и", "фа": "фа-а",
                          "соль": "со-оль", "ля": "ля-а",
                          "си": "си-и"}[tone]
        else:
            self._tone = tone

    def __str__(self):
        return self._tone
