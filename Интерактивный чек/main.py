from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QTableWidgetItem
from csv import reader
from operator import mul
from functools import reduce


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.request_filename_and_fill_table)
        self.tableWidget.clicked.connect(self.update_table)

    def fill_table(self, header, data):
        header.append("Quantity")
        data.append(["Итого", "0"])
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        self.tableWidget.setRowCount(0)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data + ["0"]):
                item = QTableWidgetItem(el)
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def load_file(self, filename):
        with open(filename, "r") as csvfile:
            csv_reader = reader(csvfile, delimiter=';', quotechar='"')
            header = next(csv_reader)
            data = tuple(line for line in csv_reader)
        return header, data

    def request_filename(self):
        fname = QFileDialog.getOpenFileName(
            self, "Загрузить файл с ценами", "", "CSV files (*.csv)")[0]
        if fname:
            return fname
        raise FileNotFoundError

    def request_filename_and_fill_table(self):
        try:
            filename = self.request_filename()
        except FileNotFoundError:
            return
        header, data = self.load_file(filename)
        data = sorted(data, key=lambda val: int(val[1]), reverse=True)
        self.fill_table(header, data)

    def update_table(self):
        total = 0
        for row in range(self.tableWidget.rowCount()):
            total += reduce(mul, map(lambda col: int(
                self.tableWidget.item(row, col).text()), (1, 2)))
        print(total)
        self.tableWidget.setItem(
            self.tableWidget.rowCount() - 1, 1, QTableWidgetItem(str(total)))
        self.tableWidget.resizeColumnsToContents()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
