from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.analyze_file)

    def analyze_file(self):
        with open("input.txt", "r") as inp:
            file_contents = inp.read()
        data = tuple(map(int, file_contents.split()))
        self.maximum = max(data)
        self.minimun = min(data)
        self.average = sum(data) / len(data)
        self.update_labels()
        self.write_output()

    def write_output(self):
        with open("output.txt", "w") as out:
            out.write(f"max = {self.maximum}\n")
            out.write(f"min = {self.minimun}\n")
            out.write(f"avg = {self.average}\n")

    def update_labels(self):
        self.label.setText(f"Минимальное число: {self.maximum}")
        self.label_2.setText(f"Максимальное число: {self.minimun}")
        self.label_3.setText(f"Среднее значение: {self.average}")


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
