import pygame
from math import pi, sin, cos


class RotatingPolygon(object):
    def __init__(self,
                 center: tuple,
                 start_angle: float,
                 radius: int,
                 angle: float,
                 outer_sides: int,
                 angle_speed: float,
                 color: pygame.color):
        self.center = center
        self.start_angle = start_angle
        self.radius = radius
        self.angle = angle
        self.outer_sides = outer_sides
        self.angle_speed = angle_speed
        self.color = color
        self.rot_angle = 0

    def update(self):
        self.points = [self.center] + [point_on_circle(self.center, self.radius,
                                                       self.start_angle +
                                                       self.rot_angle +
                                                       self.angle * mul)
                                       for mul in range(self.outer_sides + 1)]
        self.rot_angle += self.angle_speed
        if self.rot_angle > (2 * pi):
            self.rot_angle -= 2 * pi
        elif self.rot_angle < -(2 * pi):
            self.rot_angle += 2 * pi

    def draw(self, surface):
        print(self.rot_angle)
        pygame.draw.polygon(surface, self.color, self.points)

    def get_angle_speed(self):
        return self.angle_speed

    def set_angle_speed(self, angle_speed: float):
        self.angle_speed = angle_speed


def point_on_circle(center_coords, radius, angle):
    return (round(center_coords[0] + radius * sin(angle)),
            round(center_coords[1] + radius * cos(angle)))


def deg_to_rad(angle):
    return angle * pi / 180


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGHT = 201, 201
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
polygons = [RotatingPolygon((100, 100), start, 70, deg_to_rad(30),
                            1, deg_to_rad(0.5), pygame.Color("white")) for start in (pi - deg_to_rad(15), -deg_to_rad(75), deg_to_rad(45))]
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                for polygon in polygons:
                    polygon.set_angle_speed(
                        polygon.get_angle_speed() - deg_to_rad(0.5))
            elif event.button == 3:
                for polygon in polygons:
                    polygon.set_angle_speed(
                        polygon.get_angle_speed() + deg_to_rad(0.5))
    screen.fill(pygame.Color("black"))
    pygame.draw.circle(screen, pygame.Color("white"), (100, 100), 10)
    for polygon in polygons:
        polygon.update()
        polygon.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
