import xlrd
import csv

wb = xlrd.open_workbook('data.xlsx')
sh = wb.sheet_by_index(0)
with open("output.csv", "w") as csvfile:
    wr = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL, delimiter=";")
    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))
