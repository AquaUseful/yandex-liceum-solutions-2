import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("--count", action="store_true")
parser.add_argument("--num", action="store_true")
parser.add_argument("--sort", action="store_true")
parser.add_argument("path", type=str)

args = parser.parse_args()
try:
    with open(args.path, "r") as f:
        lines = list(map(lambda line: line.strip(), f.readlines()))
except FileNotFoundError:
    print("ERROR")
    sys.exit()

if args.sort:
    lines.sort()
if args.num:
    print(*map(lambda el: f"{el[0]} {el[1]}", enumerate(lines)), sep="\n")
else:
    print(*lines, sep="\n")
if args.count:
    print("rows count:", len(lines))