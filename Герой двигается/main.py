import pygame
import os


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    image = pygame.image.load(fullname)
    return image


class Character(pygame.sprite.Sprite):
    arrow_image = load_image('creature.png')

    def __init__(self, group=None):
        super().__init__(group)
        self.image = Character.arrow_image
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0

    def update(self, keys):
        if keys[pygame.K_UP]:
            self.rect.y -= 10
        if keys[pygame.K_DOWN]:
            self.rect.y += 10
        if keys[pygame.K_LEFT]:
            self.rect.x -= 10
        if keys[pygame.K_RIGHT]:
            self.rect.x += 10


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGTH = 300, 300
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
clock = pygame.time.Clock()
character_group = pygame.sprite.Group()
character = Character(character_group)
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    keys = pygame.key.get_pressed()
    character_group.update(keys)
    screen.fill(pygame.Color("white"))
    character_group.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
