from sys import argv

try:
    if len(argv[1:]) < 2:
        raise ValueError()
    print(sum(map(int, argv[1:])))
except Exception:
    print(0)