import pygame


def main():
    size, count = map(int, input().split())
    cell = size // count
    pygame.init()
    screen = pygame.display.set_mode((size, ) * 2)
    screen.fill(pygame.Color("white"))
    for x in range(0, size, cell * 2):
        for y in range(size - cell, -cell * 2, -cell * 2):
            for shift in (0, cell):
                pygame.draw.rect(screen, pygame.Color("black"), pygame.Rect(
                    (x + shift, y - shift), (cell, ) * 2))

    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
