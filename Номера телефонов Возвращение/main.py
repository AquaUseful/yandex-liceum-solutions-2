class LengthError(Exception):
    pass


class FormatError(Exception):
    pass


def check_brackets(string: str):
    counter = 0
    for char in string:
        if char == "(":
            counter += 1
        elif char == ")":
            counter -= 1
    assert not counter


def check_brackets_count(string: str):
    assert string.count("(") <= 1


def check_length(string):
    assert len(string) == 11


def check_country_code(num: str):
    assert num.startswith("+7") or num.startswith("8")


def check_numeric(num: str):
    assert num.isnumeric()


def check_hyphens(num: str):
    assert not (num.startswith("-") or num.endswith("-") or "--" in number)


def check_number(num: str):
    clear = clear_number(num)
    try:
        check_country_code(num)
        check_hyphens(num)
        check_brackets(num)
        check_brackets_count(num)
        check_numeric(clear)
    except AssertionError:
        raise FormatError()
    try:
        check_length(clear)
    except AssertionError:
        raise LengthError()


def clear_number(num: str):
    return "".join(filter(lambda char: char not in ("+", "-", " ", "(", ")", "\t"), num))


def process_number(num: str):
    return "+7" + clear_number(num)[-10:]


number = input().strip()
try:
    check_number(number)
    print(process_number(number))
except FormatError:
    print("неверный формат")
except LengthError:
    print("неверное количество цифр")
