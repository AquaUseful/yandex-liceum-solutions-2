import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--per-day", type=float, default=0)
parser.add_argument("--per-week", type=float, default=0)
parser.add_argument("--per-month", type=float, default=0)
parser.add_argument("--per-year", type=float, default=0)
parser.add_argument(
    "--get-by", choices=("day", "month", "year"), default="day")
args = parser.parse_args()

daily_income = args.per_day + args.per_week / \
    7 + args.per_month / 30 + args.per_year / 360
period = {"day": 1, "month": 30, "year": 360}[args.get_by]
print(int(daily_income * period))
