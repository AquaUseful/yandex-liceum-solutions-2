import pygame


def main():
    width, heigth = 300, 200
    pygame.init()
    screen = pygame.display.set_mode((width, heigth))
    screen.fill(pygame.Color("white"))
    color = pygame.Color("red")
    shifted = False
    for y in range(0, heigth, 17):
        x_shift = -15 if shifted else 0
        for x in range(0, width, 32):
            x_shift = -15 if shifted else 0
            rect = pygame.Rect(x + x_shift, y, 30, 15)
            pygame.draw.rect(screen, color, rect)
        shifted = not shifted
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
