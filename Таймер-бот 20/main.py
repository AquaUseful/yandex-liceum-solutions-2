from telegram.ext import Updater, CommandHandler

TOKEN = "1097814273:AAFA4t07_RbW5L3eIonUhLYRJTTN88pwqjM"
REQUEST_KWARGS = {"proxy_url": "socks5h://127.0.0.1:9050"}


def error(update, context):
    update.message.reply_text(f"Update {update} caused {context.error}")


def start(update, context):
    update.message.reply_text("Bot started!")


def alarm(context):
    job = context.job
    context.bot.send_message(job.context, "Timer triggered")


def set_timer(update, context):
    chat_id = update.message.chat_id
    try:
        due = int(context.args[0])
        if due < 0:
            update.message.reply_text("We sorry, time can go only forward")
            return
        if "job" in context.chat_data:
            old_job = context.chat_data["job"]
            old_job.schedule_removal()
        new_job = context.job_queue.run_once(alarm, due, chat_id)
        context.chat_data["job"] = new_job
        update.message.reply_text("Timer set!")
    except (IndexError, ValueError):
        update.message.reply_text("Bad usage")

def unset(update, context):
    if "job" not in context.chat_data:
        update.message.reply_text("Timer is not set!")
        return
    job = context.chat_data["job"]
    job.schedule_removal()
    del context.chat_data["job"]
    update.message.reply_text("Timer deleted!")

def main():
    updater = Updater(
        TOKEN, request_kwargs=REQUEST_KWARGS, use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("set_timer", set_timer, pass_args=True, pass_chat_data=True, pass_job_queue=True))
    dp.add_handler(CommandHandler("unset", unset, pass_chat_data=True))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
