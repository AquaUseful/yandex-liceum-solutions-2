from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QFileDialog
from PyQt5.QtGui import QPixmap
from PIL import Image, ImageQt


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.pushButton.clicked.connect(self.load_image)
        self.horizontalSlider.valueChanged.connect(self.update_label)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def load_image(self):
        try:
            path = self.request_open_path(
                "Выберите изображение", "Image files (*.png *.xpm *.jpg *.jpeg)")
        except FileNotFoundError:
            return
        self.image = Image.open(path).convert("RGBA")
        self.update_label()

    def process_image(self):
        self.image.putalpha(self.horizontalSlider.value())

    @staticmethod
    def image_to_pixmap(image):
        return QPixmap.fromImage(ImageQt.ImageQt(image))

    def update_label(self):
        self.process_image()
        pixmap = self.image_to_pixmap(self.image)
        self.label.setPixmap(pixmap)


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
