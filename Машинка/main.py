import sys
from PyQt5.QtWidgets import QWidget, QApplication, QLabel
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.setMouseTracking(True)
        self.flag = True
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle('Машинка')
        self.pixmap1 = QPixmap('car1.png')
        self.pixmap1 = self.pixmap1.scaled(
            70, 70, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.pixmap2 = QPixmap('car2.png')
        self.pixmap2 = self.pixmap2.scaled(
            70, 70, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.image = QLabel(self)
        self.image.move(80, 60)
        self.image.resize(70, 70)
        self.image.setPixmap(self.pixmap1)
        self.show()

    def mouseMoveEvent(self, event):
        if event.x() + 70 < 300 and event.y() + 70 < 300:
            self.image.move(event.x(), event.y())

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.flag = not self.flag
            if self.flag:
                self.image.setPixmap(self.pixmap1)
            else:
                self.image.setPixmap(self.pixmap2)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
