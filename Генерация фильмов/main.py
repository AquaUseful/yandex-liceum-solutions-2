import sqlite3
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("MainWindow.ui", self)
        self.con = sqlite3.connect("/home/catsatan/Загрузки/films.db")
        self.pushButton.clicked.connect(self.update_result)
        self.pushButton_2.clicked.connect(self.clear_table)
        self.modified = {}
        self.titles = None

    def update_result(self):
        cur = self.con.cursor()
        # Получили результат запроса, который ввели в текстовое поле
        result = cur.execute("Select * from films WHERE id=?",
                             (self.spinBox.text(),)).fetchall()
        if not result:
            self.clear_table()
            return
        # Заполнили размеры таблицы
        self.tableWidget.setRowCount(len(result))
        self.tableWidget.setColumnCount(len(result[0]))
        self.titles = [description[0] for description in cur.description]
        # Заполнили таблицу полученными элементами
        for i, elem in enumerate(result):
            for j, val in enumerate(elem):
                self.tableWidget.setItem(i, j, QTableWidgetItem(str(val)))
        self.tableWidget.resizeColumnsToContents()
        self.update_db()

    def update_db(self):
        cur = self.con.cursor()
        _, title, year, genre, duration = cur.execute(
            f"SELECT * FROM Films WHERE id = {self.spinBox.text()}").fetchone()
        cur.execute(f"DELETE FROM Films WHERE id = {self.spinBox.text()}")
        cur.execute(
            f"INSERT INTO Films(title, year, genre, duration) VALUES('{title[::-1]}', {year + 1000}, {genre}, {duration // 2})")
        self.con.commit()
        cur.close()

    def clear_table(self):
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)


app = QApplication(sys.argv)
ex = MyWidget()
ex.show()
sys.exit(app.exec_())
