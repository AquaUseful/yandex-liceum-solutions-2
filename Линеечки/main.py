import pygame
import collections

COLOR_MAP = {0: "black", 1: "blue", 2: "red"}


class Board(object):
    def __init__(self, width: int, height: int, fillval: int = 0) -> None:
        self.width = width
        self.height = height
        self.board = [[fillval] * height for _ in range(width)]
        self.set_view(10, 10, 20)

    def set_view(self, left: int, top: int, cell_size: int) -> None:
        self.left = left
        self.top = top
        self.cell_size = cell_size

    def get_pixel_size(self) -> tuple:
        return (self.cell_size * self.width, self.cell_size * self.height)

    def get_pixel_width(self) -> int:
        return self.cell_size * self.width

    def get_pixel_height(self) -> int:
        return self.cell_size * self.height

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.height):
                fill_color = pygame.Color(COLOR_MAP[self.board[x][y]])
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.rect(screen, fill_color, rect, 0)
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)

    def relative_pos(self, pos: tuple) -> tuple:
        return (pos[0] - self.left, pos[1] - self.top)

    def get_click(self, pos):
        cell = self.get_cell(pos)
        self.on_click(cell)

    def on_click(self, cell):
        print(cell)

    def get_cell(self, pos: tuple) -> tuple:
        if all(map(lambda pos, maximum: 0 < pos < maximum, self.relative_pos(pos), self.get_pixel_size())):
            return tuple(coord // self.cell_size for coord in self.relative_pos(pos))
        else:
            return None


class Lines(Board):
    def __init__(self, width, height, fillval=0):
        super().__init__(width, height, fillval=fillval)
        self.player_pos = None

    def on_click(self, cell):
        if cell is None:
            return
        x, y = cell
        cellval = self.board[x][y]
        if cellval == 0 and self.player_pos is None:
            self.put_blue(cell)
        elif cellval == 0:
            self.move_red(cell)
        elif cellval == 1 and self.player_pos is None:
            self.player_pos = cell
            self.put_red(cell)
        elif cellval == 2:
            self.player_pos = None
            self.put_blue(cell)

    def put_blue(self, cell):
        x, y = cell
        self.board[x][y] = 1

    def put_red(self, cell):
        x, y = cell
        self.board[x][y] = 2

    def has_path(self, x1, y1, x2, y2):
        queue = collections.deque(((x1, y1), ))
        visited = []
        while queue:
            cell = queue.popleft()
            visited.append(cell)
            if cell == (x2, y2):
                return True
            unvisited = tuple(filter(lambda neigh: neigh not in visited,
                                     self.get_neighbours(cell)))
            queue.extend(unvisited)
            visited.extend(unvisited)
            print(queue)
        return False

    def get_neighbours(self, cell):
        x, y = cell
        neighbours = []
        for shift_x, shift_y in ((1, 0), (-1, 0), (0, 1), (0, -1)):
            new_x, new_y = x + shift_x, y + shift_y
            if 0 <= new_x < self.width and \
                    0 <= new_y < self.height and \
                    self.board[new_x][new_y] == 0:
                neighbours.append((new_x, new_y))
        return tuple(neighbours)

    def clear(self, cell):
        x, y = cell
        self.board[x][y] = 0

    def move_red(self, cell):
        x, y = cell
        if self.has_path(*self.player_pos, x, y):
            self.put_blue((x, y))
            self.clear(self.player_pos)
            self.player_pos = None

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.height):
                fill_color = pygame.Color(COLOR_MAP[self.board[x][y]])
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.ellipse(screen, fill_color, rect, 0)
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)


running = True
run_fps = 100
screen_height, screen_width = 520, 520

board = Lines(10, 10, 0)
board.set_view(10, 10, 50)

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                board.get_click(event.pos)
    screen.fill(pygame.Color("black"))
    board.render()
    pygame.display.flip()
    clock.tick(run_fps)
pygame.quit()
