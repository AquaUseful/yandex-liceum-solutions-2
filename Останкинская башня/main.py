import requests
import math


def lonlat_distance(a, b):
    degree_to_meters_factor = 111 * 1000
    a_lon, a_lat = a
    b_lon, b_lat = b
    radians_lattitude = math.radians((a_lat + b_lat) / 2.)
    lat_lon_factor = math.cos(radians_lattitude)
    dx = abs(a_lon - b_lon) * degree_to_meters_factor * lat_lon_factor
    dy = abs(a_lat - b_lat) * degree_to_meters_factor
    distance = math.sqrt(dx * dx + dy * dy)
    return distance


antenna1_height = 525

server_address = "http://geocode-maps.yandex.ru/1.x/"
api_key = "40d1649f-0493-4b70-98ba-98533de7710b"

addresses = ["Москва, улица Академика Королёва, 15к1"]
addresses.append(input("Введите адрес: "))

coords = []
for address in addresses:
    params = {
        "apikey": api_key,
        "geocode": address,
        "format": "json"
    }
    response = requests.get(server_address, params)
    json_resp = response.json()
    if response:
        coords.append(tuple(map(float, json_resp["response"]["GeoObjectCollection"]
                                ["featureMember"][0]["GeoObject"]["Point"]["pos"].split())))

distance = lonlat_distance(*coords)
print("Расстояние до антенны:", round(distance, 3), "м")
antenna2_height = (math.sqrt(antenna1_height) - distance / 3600) ** 2
print("Необходимая высота антенны:", round(antenna2_height, 2), "м")
