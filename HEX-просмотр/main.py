from sys import stdin
from itertools import count
print("          00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f\n")
for line_num in count(0, 16):
    line = stdin.buffer.read(16)
    if not line:
        break
    print(f"""{line_num:0>6x}\
    {" ".join(map(lambda byte: f'{byte:0>2x}', line)):<47} \
    {"".join(map(lambda byte: f'{byte:c}' if f'{byte:c}'.isprintable() else '.', line))}""")
