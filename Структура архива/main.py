import zipfile

with zipfile.ZipFile("input.zip") as archive:
    for path in archive.namelist():
        depth = path.strip("/").count("/")
        name = path.strip("/").split("/")[-1]
        print("  " * depth + name)
