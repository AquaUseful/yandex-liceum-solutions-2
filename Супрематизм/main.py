import sys
from PyQt5.QtWidgets import QWidget, QApplication, QLabel
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QColor
from random import randint
from math import sin, cos, pi


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.setMouseTracking(True)
        self.key = 0
        self.flag = False
        self.coords = ()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle('РљРѕРѕСЂРґРёРЅР°С‚С‹')
        self.show()

    def paintEvent(self, event):
        if self.flag:
            self.qp = QPainter()
            self.qp.begin(self)
            self.draw(self.qp, self.coords[0], self.coords[1])
            self.qp.end()

    def mouseMoveEvent(self, event):
        self.coords = event.x(), event.y()

    def mousePressEvent(self, event):
        self.coords = event.x(), event.y()
        if (event.button() == Qt.LeftButton):
            self.flag = True
            self.key = 1
        elif (event.button() == Qt.RightButton):
            self.flag = True
            self.key = 2
        self.update()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.flag = True
            self.key = 3
        self.update()

    def draw(self, qp, x, y):
        color = QColor(randint(0, 255), randint(0, 255), randint(0, 255))
        qp.setBrush(color)
        r = randint(1, 100)
        if self.key == 1:
            qp.drawEllipse(x - r, y - r, 2 * r, 2 * r)
        elif self.key == 2:
            qp.drawRect(x - r, y - r, 2 * r, 2 * r)
        elif self.key == 3:
            coords = gen_coords(r, 3, (x, y), -pi / 2)
            for i in range(len(coords)):
                qp.drawLine(coords[i - 1][0], coords[i - 1][1],
                            coords[i][0], coords[i][1])
        self.flag = False


def gen_coords(radius, sides, shift, rotation):
    return tuple((round(cos(2 * pi * k / sides + rotation) * radius + shift[0]),
                  round(sin(2 * pi * k / sides + rotation) * radius + shift[1])) for k in range(sides))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
