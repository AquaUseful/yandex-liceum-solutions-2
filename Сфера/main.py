import pygame


def main():
    count = int(input())
    pygame.init()
    screen = pygame.display.set_mode((300, ) * 2)
    screen.fill(pygame.Color("black"))
    dist = 300 // count
    rect_coords = 0
    rect_size = 300
    for _ in range(count):
        pygame.draw.ellipse(screen, pygame.Color("white"),
                            pygame.Rect(0, rect_coords, 300, rect_size), 1)
        pygame.draw.ellipse(screen, pygame.Color("white"),
                            pygame.Rect(rect_coords, 0, rect_size, 300), 1)
        rect_coords += dist // 2
        rect_size -= dist
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
