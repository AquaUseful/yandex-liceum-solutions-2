def reverse():
    with open("input.dat", "rb") as inp:
        with open("output.dat", "wb") as out:
            out.write(inp.read()[::-1])
