from typing import Iterable
import datetime
from random import randint
from itertools import chain
from functools import reduce
from operator import add
from docx import Document
import xlsxwriter


class Ticket(object):
    def __init__(self, number: int):
        self.number = number

    def __str__(self):
        return f"Ticket with number: {self.number}"

    def __repr__(self):
        return f"Ticket({self.number})"


class Film(object):
    def __init__(self, title: str, duration: int):
        self.title = title
        self.duration = duration

    def get_title(self):
        return self.title

    def get_duration(self):
        return self.duration

    def __eq__(self, other):
        return self.title == other.title and self.duration == other.duration

    def __str__(self):
        return self.title

    def __repr__(self):
        return f"Film('{self.title}', {self.duration})"

    def __hash__(self):
        return hash((self.title, self.duration))


class Session(object):
    def __init__(self, film: Film, datetime: datetime.datetime, ticket_price: int):
        self.film = film
        self.datetime = datetime
        self.ticket_price = ticket_price

    def get_film(self):
        return self.film

    def get_datetime(self):
        return self.datetime

    def get_datetime_end(self):
        return self.datetime + datetime.timedelta(minutes=self.film.get_duration())

    def __hash__(self):
        return hash((self.film, self.datetime, self.ticket_price))

    def __str__(self):
        return f"Session (Film: {self.film.get_title()}, Start: {self.datetime.strftime('%x %X')})"

    def get_ticket_price(self):
        return self.ticket_price


class Cinema(object):
    def __init__(self, title: str, halls: Iterable = []):
        self.title = title
        self.halls = list(halls)

    def add_hall(self, hall):
        self.halls.append(hall)

    def find_nearest_session(self, session):
        sessions = tuple(filter(lambda el: el is not None, map(
            lambda hall: hall.find_nearest_session(session), self.halls)))
        if sessions:
            return min(sessions, key=lambda val: val.get_datetime())
        return None

    def get_halls(self):
        return self.halls

    def delete_hall(self, hall):
        del self.halls[self.halls.index(hall)]

    def __str__(self):
        return self.title

    def get_sessions(self):
        return reduce(add, map(lambda hall: hall.get_sessions(), self.halls))

    def get_hall(self, session: Session):
        for hall in self.halls:
            if hall.is_session_in_hall(session):
                return hall

    def tickets_count_on_day(self, date: datetime.date):
        return sum(map(lambda hall: hall.tickets_count_on_day(date), self.halls))


class Hall(object):
    def __init__(self, seats_config: tuple, sessions: Iterable = []):
        self.seats_config = seats_config
        seatmap = tuple([None] * seats_config[1]
                        for _ in range(seats_config[0]))
        self.sessions = {session: seatmap for session in sessions}

    def gen_seatmap(self):
        return tuple([None] * self.seats_config[1] for _ in range(self.seats_config[0]))

    def get_seats_config(self):
        return self.seats_config

    def add_session(self, session):
        new_session_film = session.get_film()
        new_session_start = session.get_datetime()
        new_session_end = session.get_datetime_end()
        sessions_starts_ends = tuple((session.get_datetime(), session.get_datetime_end())
                                     for session in self.sessions)
        collision = any(map(lambda el: el[0] <= new_session_start <= el[1] or
                            el[0] <= new_session_end <= el[1],
                            sessions_starts_ends)) or \
            any(map(lambda el: new_session_start <= el[0] <= new_session_end or
                    new_session_start <= el[1] <= new_session_end,
                    sessions_starts_ends))
        if not collision:
            self.sessions.update({session: self.gen_seatmap()})
            return True
        return False

    def print_hall(self, session=None):
        if session is None:
            print(self.sessions)
        else:
            print(
                "   " + "   ".join(map(str, range(1, len(self.sessions[session][0]) + 1))))
            print(*map(lambda row: f"{(row[0] + 1)} \
                {' '.join(map(lambda el: '[ ]' if el is None else '[*]', row[1]))}", enumerate(
                self.sessions[session])), sep='\n')

    def get_seatmap(self, session):
        return self.sessions[session]

    def add_ticket(self, session: Session, seat: tuple, ticket: Ticket):
        if self.sessions[session][seat[0]][seat[1]] is None:
            self.sessions[session][seat[0]][seat[1]] = ticket
            return True
        return False

    def find_nearest_session(self, film: Film):
        sessions = tuple(filter(lambda session: session.get_film() == film and
                                any(map(lambda row: check_for_none(row),
                                        self.sessions[session])) and
                                session.get_datetime() > datetime.datetime.now(),
                                self.sessions.keys()))
        if sessions:
            return min(sessions, key=lambda val: val.get_datetime())
        return None

    def get_sessions(self):
        return tuple(self.sessions.keys())

    def __str__(self):
        return f"Hall (size: {self.seats_config})"

    def delete_session(self, session: Session):
        del self.sessions[session]

    def sell_ticket(self, session, seat_pos, ticket):
        if self.sessions[session][seat_pos[0]][seat_pos[1]] is None:
            self.sessions[session][seat_pos[0]][seat_pos[1]] = ticket
            return True
        return False

    def is_session_in_hall(self, session: Session):
        return session in self.sessions

    def tickets_count_on_day(self, date: datetime.date):
        return sum(map(lambda tick: 0 if tick is None else 1,
                       chain(*chain(*chain(map(lambda sess: sess[1],
                                               filter(lambda session:
                                                      session[0].get_datetime(
                                                      ).date() == date,
                                                      self.sessions.items())))))))


def check_for_none(iterable: Iterable):
    return any(map(lambda el: el is None, iterable))


def main_menu(filmlist: list, cinemalist: list):
    variants = ("Film menu", "Cinema menu",
                "Ticket menu", "Search", "Report menu", "Exit")
    while True:
        sel = single_choice(variants, "Choose action: ")[0]
        if sel == 0:
            film_menu(filmlist)
        elif sel == 1:
            cinema_menu(cinemalist, filmlist)
        elif sel == 2:
            ticket_menu(cinemalist)
        elif sel == 3:
            search_menu(cinemalist, filmlist)
        elif sel == 4:
            report_menu(cinemalist)
        elif sel == 5:
            break


def single_choice(iterable: Iterable, promt: str = ""):
    variants = tuple(iterable)
    print(promt)
    print(*map(lambda el: f"{el[0] + 1}: {el[1]}",
               enumerate(variants)), sep='\n')
    while True:
        try:
            selection_num = int(input("> "))
            selection = variants[selection_num - 1]
        except ValueError:
            print("Error, input only number!")
            continue
        except IndexError:
            print("Range error!")
            continue
        break
    return (selection_num - 1, selection)


def int_input(promt: str) -> int:
    while True:
        val = input(promt)
        if val.isnumeric():
            return int(val)
        print("Only integers are allowed!")


def multiple_choice(iterable: Iterable, promt: str = ""):
    variants = tuple(iterable)
    print(promt)
    print(*map(lambda el: f"{el[0] + 1}: {el[1]}",
               enumerate(variants)), sep='\n')
    while True:
        try:
            selection_nums = tuple(
                map(lambda el: int(el) - 1, input("> ").split()))
            selection = tuple(variants[sel - 1] for sel in selection_nums)
        except ValueError:
            print("Error, input only numbers and spaces!")
            continue
        except IndexError:
            print("Range error!")
            continue
        break
    return (selection_nums, selection)


def film_menu(filmlist):
    variants = ("Show all films", "Add film", "Delete film", "Back")
    while True:
        sel = single_choice(variants, "Choose action: ")[0]
        if sel == 0:
            print("\nAll films, one per line:")
            print(*filmlist, sep='\n')
            print()
        elif sel == 1:
            while True:
                title = input("Input film title: ")
                if title:
                    break
            while True:
                duration = int_input("Input film duration in minutes: ")
                if duration > 0:
                    break
            filmlist.append(Film(title, duration))
        elif sel == 2:
            print("Which films you want to delete: ")
            for el in sorted(multiple_choice(filmlist)[0], reverse=True):
                del filmlist[el]
        elif sel == 3:
            break


def cinema_menu(cinemalist, filmlist):
    variants = ("Show all cinema", "Add cinema",
                "Delete cinema", "Edit cinema", "Back")
    while True:
        sel = single_choice(variants, "Choose action: ")[0]
        if sel == 0:
            print("\nAll cinema, one per line:")
            print(*cinemalist, sep='\n')
            print()
        elif sel == 1:
            while True:
                title = input("Input cinema title: ")
                if title:
                    break
            cinemalist.append(Cinema(title))
        elif sel == 2:
            print("Which cinema you want to delete: ")
            for el in sorted(multiple_choice(cinemalist)[0], reverse=True):
                del cinemalist[el]
        elif sel == 3:
            print("Select cinema to edit")
            sel = single_choice(cinemalist)[1]
            edit_cinema_menu(sel, filmlist)
        elif sel == 4:
            break


def edit_cinema_menu(cinema, filmlist):
    variants = ("Show all halls", "Add hall", "Delete hall",
                "Show all sessions", "Add session", "Delete session", "Back")
    while True:
        sel = single_choice(variants, "Choose action: ")[0]
        if sel == 0:
            print("\nAll halls, one per line:")
            print(*cinema.get_halls(), sep='\n')
            print()
        elif sel == 1:
            while True:
                try:
                    size = tuple(map(int, input("Input hall size\
                         (2 integers, space separated): ").split()))
                except ValueError:
                    continue
                if len(size) == 2:
                    break
            new_hall = Hall(size)
            cinema.add_hall(new_hall)
        elif sel == 2:
            print("Which hall you want to delete: ")
            cinema.delete_hall(single_choice(cinema.get_halls())[1])
        elif sel == 3:
            for hall in cinema.get_halls():
                print(f"Sessions in {hall}, one per line:")
                print(*hall.get_sessions(), sep='\n')
        elif sel == 4:
            if not cinema.get_halls():
                print("You must add hall at first!")
                continue
            print("Select a hall for a session")
            hall = single_choice(cinema.get_halls())[1]
            if not filmlist:
                print("You must add at least one film!")
                continue
            print("Select a film for a session")
            film = single_choice(filmlist)[1]
            while True:
                try:
                    date = map(int, input(
                        "Input date (yyyy.mm.dd): ").split('.'))
                    time = map(int, input(
                        "Input time (hh:mm): ").strip().split(':'))
                    dt = datetime.datetime(*date, *time)
                except ValueError:
                    continue
                break
            price = int_input("Input ticket price: ")
            session = Session(film, dt, price)
            result = hall.add_session(session)
            if not result:
                print("This session collides with other!")
        elif sel == 5:
            if not cinema.get_halls():
                print("You must add hall at first!")
                continue
            print("Select a hall to delete from")
            hall = single_choice(cinema.get_halls())[1]
            if not hall.get_sessions():
                print("This hall has no sessions!")
            sessions = multiple_choice(
                hall.get_sessions(), "Select a session(s) to delete")[1]
            for session in sessions:
                hall.delete_session(session)
        elif sel == 6:
            break


def ticket_menu(cinemalist: list):
    variants = ("Check available seats", "Sell ticket", "Back")
    while True:
        sel = single_choice(variants, "Choose action: ")[0]
        if sel == 0:
            if not cinemalist:
                print("Add cinema first!")
                continue
            cinema = single_choice(cinemalist, "Select a cinema to check: ")[1]
            if not cinema.get_sessions():
                print("There are no sessions in this cinema")
                pass
            session = single_choice(
                cinema.get_sessions(), "Select a session to check")[1]
            hall = cinema.get_hall(session)
            print(f"Seat map for {session} in {hall}")
            hall.print_hall(session)
        elif sel == 1:
            if not cinemalist:
                print("Add cinema first!")
                continue
            cinema = single_choice(
                cinemalist, "Select a cinema to sell ticket: ")[1]
            if not cinema.get_sessions():
                print("There are no sessions in this cinema")
                pass
            session = single_choice(
                cinema.get_sessions(), "Select a session to sell ticket")[1]
            hall = cinema.get_hall(session)
            while True:
                print("Select seat position: ")
                hall.print_hall(session)
                try:
                    row, col = map(int, input(
                        "Input seat position (2 integers, space separated): ").split())
                    print(row, col)
                except ValueError:
                    continue
                print(hall.get_seats_config())
                if 0 <= row - 1 < hall.get_seats_config()[0] and \
                        0 <= col - 1 < hall.get_seats_config()[1]:
                    break
                print("Invalid number!")
            result = hall.add_ticket(
                session, (row - 1, col - 1), Ticket(randint(1, 1000000)))
            if not result:
                print("This place is already taken")
                return
            print(f"Ticket price is: {session.get_ticket_price()}")
        elif sel == 2:
            break


def search_menu(cinemalist, filmlist):
    if not filmlist:
        print("Add film at first!")
        return
    sel = single_choice(filmlist, "Select film to search: ")[1]
    search_dict = {cinema.find_nearest_session(
        sel): cinema for cinema in cinemalist}
    print(search_dict)
    nearest_session = min(search_dict)
    if nearest_session is None:
        print("No sessions found")
        return
    nearest_cinema = search_dict[nearest_session]
    nearest_hall = nearest_cinema.get_hall(nearest_session)
    print("Session found!")
    print(f"In cinema {nearest_cinema}")
    print(f"In {nearest_hall}")
    print(nearest_session)


def report_menu(cinemalist):
    if not cinemalist:
        print("Add at least one cinema!")
        return
    variants = ("docx report", "xlsx report", "Back")
    sel = single_choice(variants, "Choose action: ")[0]
    if sel == 0:
        make_docx_report(cinemalist)
        print("Report 'report.docx' has been created!")
    elif sel == 1:
        make_xlsx_report(cinemalist)
        print("Report 'report.xlsx' has been created!")
    elif sel == 2:
        return


def make_xlsx_report(cinemalist):
    min_date = datetime.date.today() - datetime.timedelta(days=30)
    max_date = datetime.date.today()
    with xlsxwriter.Workbook("report.xlsx") as wb:
        worksheet = wb.add_worksheet()
        for col, cinema in enumerate(cinemalist):
            worksheet.write(0, col + 1, str(cinema))
        curr_date = min_date
        row = 1
        datetime_format = wb.add_format({'num_format': 'dd.mm.yy'})
        while curr_date < max_date:
            worksheet.write(row, 0, curr_date, datetime_format)
            for col, cinema in enumerate(cinemalist):
                worksheet.write(
                    row, col + 1, cinema.tickets_count_on_day(curr_date))
            curr_date += datetime.timedelta(days=1)
            row += 1


def make_docx_report(cinemalist):
    min_datetime = datetime.datetime.now()
    max_datetime = datetime.datetime.now() + datetime.timedelta(days=30)
    doc = Document()
    doc.add_heading("Timetable for the next 30 days", level=0)
    for cinema in cinemalist:
        doc.add_heading(f"Cinema '{cinema}'", level=1)
        for session in cinema.get_sessions():
            if min_datetime < session.get_datetime() < max_datetime:
                doc.add_paragraph(
                    f"'{session.get_film()}' at {session.get_datetime().strftime('%x %X')}'\
(Ticket price - {session.get_ticket_price()})",
                    style='List Bullet')
    doc.save("report.docx")


def main():
    films = []
    cinema = []
    main_menu(films, cinema)


if __name__ == "__main__":
    main()
