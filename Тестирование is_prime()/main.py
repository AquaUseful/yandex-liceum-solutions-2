from yandex_testing_lesson import is_prime


def testing_function(func):
    test_data = ((1, None),
                 (0, None),
                 (-1, None),
                 (-666, None),
                 (2, True),
                 (3, True),
                 (4, False),
                 (13, True),
                 (666, False),
                 (137, True),
                 (1000, False))
    for input_data, correct_data in test_data:
        try:
            returned = func(input_data)
        except ValueError as exc:
            if correct_data is not None:
                break
        else:
            if returned != correct_data:
                break
    else:
        print("YES")
        return
    print("NO")


testing_function(is_prime)
