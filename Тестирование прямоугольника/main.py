import pytest
from yandex_testing_lesson import Rectangle


def test_init_typeerr():
    with pytest.raises(TypeError):
        Rectangle("", "")
    with pytest.raises(TypeError):
        Rectangle("", 5)
    with pytest.raises(TypeError):
        Rectangle(5, "")
    with pytest.raises(TypeError):
        Rectangle([], "")
    with pytest.raises(TypeError):
        Rectangle("", [])
    with pytest.raises(TypeError):
        Rectangle({4}, "")
    with pytest.raises(TypeError):
        Rectangle("", {56})
    with pytest.raises(TypeError):
        Rectangle(None, "")
    with pytest.raises(TypeError):
        Rectangle("", None)


def test_init_valueerr():
    with pytest.raises(ValueError):
        Rectangle(-5, 5)
    with pytest.raises(ValueError):
        Rectangle(57, -0.48)
    with pytest.raises(ValueError):
        Rectangle(-55, -45)


def test_get_area():
    rect = Rectangle(5, 5)
    assert rect.get_area() == 25
    rect = Rectangle(25.48, 78.57)
    assert rect.get_area() == 25.48 * 78.57
    rect = Rectangle(0, 10)
    assert rect.get_area() == 0
    rect = Rectangle(30, 0)
    assert rect.get_area() == 0


def test_get_perimeter():
    rect = Rectangle(5, 5)
    assert rect.get_perimeter() == 20
    rect = Rectangle(25.48, 78.57)
    assert rect.get_perimeter() == (25.48 + 78.57) * 2
    rect = Rectangle(0, 10)
    assert rect.get_perimeter() == 20
    rect = Rectangle(30, 0)
    assert rect.get_perimeter() == 60
