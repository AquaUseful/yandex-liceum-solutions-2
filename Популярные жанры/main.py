import sqlite3

db_name = input()
db = sqlite3.connect(db_name)
cur = db.cursor()
res = cur.execute(
    """SELECT title FROM genres
    WHERE id IN (
    SELECT DISTINCT genre FROM Films
        WHERE year IN (2010, 2011))
"""
)
print(*map(lambda val: val[0], res), sep="\n")
cur.close()
