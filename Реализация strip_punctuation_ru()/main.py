def strip_punctuation_ru(data: str):
    data = "".join(map(lambda char: " " if char in (
        ",", ".", "?", "!", ":", ";") else char, data))
    data = data.replace(" - ", " ")
    while "  " in data:
        data = data.replace("  ", " ")
    return data


print(strip_punctuation_ru(input()))
