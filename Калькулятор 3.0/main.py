import argparse

parser = argparse.ArgumentParser()
parser.add_argument("numbers", nargs="*")

args = parser.parse_args()

if not args.numbers:
    print("NO PARAMS")
elif len(args.numbers) < 2:
    print("TOO FEW PARAMS")
elif len(args.numbers) > 2:
    print("TOO MUCH PARAMS")
else:
    try:
        print(sum(map(int, args.numbers)))
    except Exception as exc:
        print(exc.__class__.__name__)