import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
import random
import datetime
import re

TOKEN = ""
GROUP_ID = 193176805
WEEKDAYS = ("Понедельник", "Вторник", "Среда", "Четверг",
            "Пятница", "Суббота", "Воскресенье")


def main():
    vk_session = vk_api.VkApi(token=TOKEN)

    longpoll = VkBotLongPoll(vk_session, GROUP_ID)
    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            msg_text = event.obj.message["text"]
            usr_id = event.obj.message["from_id"]
            vk = vk_session.get_api()
            if re.match("^(([0-9]{4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]))$", msg_text):
                year, month, day = map(int, msg_text.split("-"))
                dt = datetime.date(year, month, day)
                msg = f"День недели в тот день: {WEEKDAYS[dt.weekday()]}"
                vk.messages.send(user_id=usr_id, message=msg,
                                 random_id=random.randint(0, 2 ** 64))
            else:
                msg = "Введите дату в формате 'ГГГГ-ММ-ДД' ('YYYY-MM-DD')"
                vk.messages.send(user_id=usr_id, message=msg,
                                 random_id=random.randint(0, 2 ** 64))


if __name__ == "__main__":
    main()
