import zipfile


def human_read_format(size):
    names = ["", "К", "М", "Г", "T"]
    step = 0
    while size >= 1024:
        step += 1
        size /= 1024
    return str(round(size)) + names[step] + "Б"


with zipfile.ZipFile("input.zip") as archive:
    for info in archive.infolist():
        path = info.filename
        size = info.file_size
        depth = path.strip("/").count("/")
        name = path.strip("/").split("/")[-1]
        print("  " * depth + name +
              (" " + human_read_format(size) if path[-1] != "/" else ""))
