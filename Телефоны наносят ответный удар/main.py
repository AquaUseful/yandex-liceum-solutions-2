class LengthError(Exception):
    pass


class FormatError(Exception):
    pass


class OperatorError(Exception):
    pass


def check_brackets(string: str):
    counter = 0
    for char in string:
        if char == "(":
            counter += 1
        elif char == ")":
            counter -= 1
    assert not counter


def check_brackets_count(string: str):
    assert string.count("(") <= 1


def check_length(string):
    assert len(string) == 11


def check_country_code(num: str):
    assert num.startswith("+7") or num.startswith("8")


def check_numeric(num: str):
    assert num.isnumeric()


def check_hyphens(num: str):
    assert not (num.startswith("-") or num.endswith("-") or "--" in number)


def check_operator(num: str):
    code = int(num[1:4])
    assert 910 <= code <= 919 or \
        980 <= code <= 989 or \
        920 <= code <= 939 or \
        902 <= code <= 906 or \
        960 <= code <= 969


def check_number(num: str):
    clear = clear_number(num)
    try:
        check_country_code(num)
        check_hyphens(num)
        check_brackets(num)
        check_brackets_count(num)
        check_numeric(clear)
    except AssertionError:
        raise FormatError()
    try:
        check_length(clear)
    except AssertionError:
        raise LengthError()
    try:
        check_operator(clear)
    except AssertionError:
        raise OperatorError()


def clear_number(num: str):
    return "".join(filter(lambda char: char not in ("+", "-", " ", "(", ")", "\t"), num))


def process_number(num: str):
    return "+7" + clear_number(num)[-10:]


number = input().strip()
try:
    check_number(number)
    print(process_number(number))
except FormatError:
    print("неверный формат")
except LengthError:
    print("неверное количество цифр")
except OperatorError:
    print("не определяется оператор сотовой связи")
