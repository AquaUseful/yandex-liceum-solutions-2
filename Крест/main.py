import pygame


def main():
    width, heigth = map(int, input().split())
    pygame.init()
    screen = pygame.display.set_mode((width, heigth))
    color = pygame.Color("#FFFFFF")
    pygame.draw.line(screen, color, (0, 0), (width, heigth), 5)
    pygame.draw.line(screen, color, (width, 0), (0, heigth), 5)
    pygame.display.flip()
    while pygame.event.wait().type != pygame.QUIT:
        pass


if __name__ == "__main__":
    main()
