import vk_api
from auth import LOGIN, PASSWORD
import datetime



def auth_handler():
    key = input("Auth code: ")
    remember = False
    return key, remember


def main():
    login, password = LOGIN, PASSWORD
    vk_session = vk_api.VkApi(login, password, auth_handler=auth_handler)
    try:
        vk_session.auth()
    except vk_api.AuthError as err:
        print(err)
        return
    vk = vk_session.get_api()
    resp = vk.wall.get(count=5, offset=0)
    if resp["items"]:
        for item in resp["items"]:
            text = item["text"]
            date = item["date"]
            dt = datetime.datetime.fromtimestamp(int(date))
            print(text + ";")
            print(f"date: {dt.date()}, time: {dt.time()}")



if __name__ == "__main__":
    main()


