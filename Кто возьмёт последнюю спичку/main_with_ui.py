from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(130, 245)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.lcdNumber = QtWidgets.QLCDNumber(self.groupBox)
        self.lcdNumber.setMinimumSize(QtCore.QSize(50, 50))
        self.lcdNumber.setObjectName("lcdNumber")
        self.gridLayout.addWidget(self.lcdNumber, 0, 0, 1, 1)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.spinBox = QtWidgets.QSpinBox(self.groupBox_2)
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(5)
        self.spinBox.setObjectName("spinBox")
        self.verticalLayout.addWidget(self.spinBox)
        self.pushButton = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        self.verticalLayout_2.addWidget(self.groupBox_2)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.verticalLayout_2.addWidget(self.pushButton_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Кто возьмёт последнюю спичку"))
        self.groupBox.setTitle(_translate("MainWindow", "Спичек в куче"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Взять спички"))
        self.pushButton.setText(_translate("MainWindow", "Взять"))
        self.pushButton_2.setText(_translate("MainWindow", "Начать заново"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.lcdNumber.display(37)
        self.pushButton.clicked.connect(self.make_move)
        self.pushButton_2.clicked.connect(self.restart)

    def restart(self):
        self.lcdNumber.display(37)
        self.spinBox.setMaximum(5)
        self.spinBox.setEnabled(True)
        self.pushButton.setEnabled(True)

    def comp_move(self):
        if self.lcdNumber.intValue() <= 5:
            take = self.lcdNumber.intValue() - 1
        elif self.lcdNumber.intValue() == 1:
            take = 1
        else:
            take = 6 - self.spinBox.value()
        self.lcdNumber.display(self.lcdNumber.intValue() - take)
        if self.lcdNumber.intValue() < 5:
            self.spinBox.setMaximum(self.lcdNumber.intValue())
        self.spinBox.setEnabled(True)
        self.pushButton.setEnabled(True)
        if self.lcdNumber.intValue() <= 0:
            self.show_messagebox("Вы проиграли!")
            self.restart()
            return

    def make_move(self):
        self.spinBox.setEnabled(False)
        self.pushButton.setEnabled(False)
        self.lcdNumber.display(
            self.lcdNumber.intValue() - self.spinBox.value())
        if self.lcdNumber.intValue() <= 0:
            self.show_messagebox("Вы проиграли!")
            self.restart()
            return
        self.comp_move()

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
