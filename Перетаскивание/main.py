import pygame


running = True
fps = 100
square_size = 100
square_coords = (0, 0)
square_move = False
pygame.init()
heigth, width = 300, 300
screen = pygame.display.set_mode((heigth, width))
clock = pygame.time.Clock()
while running:
    screen.fill(pygame.Color("black"))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if square_coords[0] <= event.pos[0] < square_coords[0] + square_size and \
                    square_coords[1] <= event.pos[1] < square_coords[1] + square_size:
                square_move = True
                move_start_coords = event.pos
        elif event.type == pygame.MOUSEBUTTONUP:
            square_move = False
        elif event.type == pygame.MOUSEMOTION:
            if square_move:
                delta = (event.pos[0] - move_start_coords[0],
                         event.pos[1] - move_start_coords[1])
                move_start_coords = event.pos
                square_coords = (square_coords[0] + delta[0],
                                 square_coords[1] + delta[1])

    pygame.draw.rect(screen, pygame.Color("green"),
                     pygame.Rect(square_coords, (square_size, ) * 2))
    pygame.display.flip()
    clock.tick(fps)
