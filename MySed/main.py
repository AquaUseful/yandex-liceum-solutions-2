from sys import stdin, stdout, argv
if len(argv) == 1:
    print("Usage: python [SCRIPT NAME] [COMMAND]")
    exit()
if "--help" in argv:
    print("Reads data from stdin, edits and return to stdout")
    print("Usage: python [SCRIPT NAME] [COMMAND]")
    print("Command: s/[PATTERN]/[STRING]")
command = argv[1]
cmd, patt, string = command.split("/")
for line in stdin:
    line = line.replace(patt, string)
    stdout.write(line)
