from math import sqrt


def is_prime(n):
    if n < 2:
        return False
    for divisor in range(2, int(sqrt(n)) + 1):
        if n % divisor == 0:
            return False
    return True


print("YES" if is_prime(int(input())) else "NO")
