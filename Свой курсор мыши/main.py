import pygame
import os


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    image = pygame.image.load(fullname)
    return image


class Arrow(pygame.sprite.Sprite):
    arrow_image = load_image('arrow.png')

    def __init__(self, group=None):
        super().__init__(group)
        self.image = Arrow.arrow_image
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0

    def update(self, event):
        self.rect.x, self.rect.y = event.pos


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGTH = 500, 500
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
clock = pygame.time.Clock()
cursor_group = pygame.sprite.Group()
cursor = Arrow(cursor_group)
pygame.mouse.set_visible(False)
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEMOTION:
            cursor_group.update(event)

    screen.fill(pygame.Color("black"))
    if pygame.mouse.get_focused():
        cursor_group.draw(screen)
    pygame.display.flip()
    clock.tick(FPS)
pygame.quit()
