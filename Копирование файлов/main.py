import argparse
import itertools

parser = argparse.ArgumentParser()
parser.add_argument("--upper", action="store_true")
parser.add_argument("--lines", type=int)
parser.add_argument("source", type=str)
parser.add_argument("destination", type=str)
args = parser.parse_args()

with open(args.source, "r") as f:
    lines = f.readlines()
    lines = tuple(itertools.islice(lines, args.lines))
if args.upper:
    lines = map(lambda line: line.upper(), lines)
with open(args.destination, "w") as f:
    f.writelines(lines)
