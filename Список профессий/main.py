import flask

app = flask.Flask(__name__)


@app.route("/<pagename>")
@app.route("/index/<pagename>")
def index(pagename):
    return flask.render_template("base.htm", title=pagename)


@app.route("/list_prof/<list_type>")
def list_prof(list_type):
    jobs = ("инженер-исследователь",
            "пилот",
            "строитель",
            "экзобиолог",
            "врач",
            "инженер по терраформированию",
            "климатолог",
            "специалист по радиационной защите",
            "астрогеолог", "гляциолог",
            "инженер жизнеобеспечения",
            "метеоролог",
            "оператор марсохода",
            "киберинженер",
            "штурман",
            "пилот дронов")
    return flask.render_template("list_prof.htm", list_type=list_type, jobs=jobs)


if __name__ == "__main__":
    app.run(host="::1", port=8080, debug=True)
