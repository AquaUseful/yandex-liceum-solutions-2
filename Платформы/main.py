import pygame
import os
import random


class PygameFillingRect(object):
    def __init__(self, coords, size, color, border=0):
        self.coords = coords
        self.size = size
        self.color = color
        self.border = border
        self._prepare()

    def _prepare(self):
        self.rect = pygame.Rect(self.coords, self.size)

    def render(self, surface):
        pygame.draw.rect(surface, self.color, self.rect, self.border)

    def move(self, coords_delta):
        self.coords = (self.coords[0] + coords_delta[0],
                       self.coords[1] + coords_delta[1])
        self._prepare()

    def update(self):
        pass

    def get_rect(self):
        return self.rect

    def colliderect(self, rect):
        return self.rect.colliderect(rect)


class Platform(PygameFillingRect):
    def __init__(self, coords):
        super().__init__(coords, (50, 10), pygame.Color("gray"), 0)


class Player(PygameFillingRect):
    def __init__(self, coords):
        super().__init__(coords, (20, 20), pygame.Color("blue"), 0)
        self.velocity = (0, 1)

    def update(self, movement, walls):
        new_pos = self.rect.copy()
        print(*map(lambda wall: new_pos.colliderect(wall.get_rect()), walls))
        new_pos = self.rect.move(self.velocity)
        if not sum(map(lambda wall: new_pos.colliderect(wall.get_rect()), walls)):
            self.move(self.velocity)
        new_pos = self.rect.move(movement)
        if not sum(map(lambda wall: new_pos.colliderect(wall.get_rect()), walls)):
            self.move(movement)


FPS = 100
SCREEN_WIDTH, SCREEN_HEIGTH = 500, 500
running = True
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
clock = pygame.time.Clock()
all_sprites = pygame.sprite.Group()
player = None
walls = []
movement = (0, 0)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                walls.append(Platform(event.pos))
            elif event.button == 3:
                player = Player(event.pos)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                movement = (10, 0)
            elif event.key == pygame.K_LEFT:
                movement = (-10, 0)
    screen.fill(pygame.Color("black"))
    for wall in walls:
        wall.render(screen)
    if player is not None:
        player.update(movement, walls)
        movement = (0, 0)
        player.render(screen)
    pygame.display.flip()
    clock.tick(FPS)

pygame.quit()
