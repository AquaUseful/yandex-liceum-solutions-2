from sys import argv, exit
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QTableWidgetItem, QMessageBox
import sqlite3
import datetime


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.db = None

    def initUi(self):
        uic.loadUi("MainWindow.ui", self)
        self.set_max_year()
        self.action_exit.triggered.connect(exit)
        self.action_open.triggered.connect(self.load_database)
        self.pushButton_add.clicked.connect(self.add_film)

    def load_genres(self):
        self.create_cursor()
        genres = self.execute_query("""SELECT title FROM genres""").fetchall()
        genres = map(lambda val: val[0], genres)
        self.comboBox_genre.addItems(genres)
        self.close_cursor()

    def get_col_names(self):
        self.create_cursor()
        self.execute_query_fetchone("SELECT * FROM Films")
        names = map(lambda val: val[0], self.cur.description)
        self.close_cursor()
        return tuple(names)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def create_cursor(self):
        self.cur = self.db.cursor()

    def execute_query(self, query):
        return self.cur.execute(query)

    def close_cursor(self):
        self.cur.close()

    def load_database(self):
        try:
            path = self.request_open_path(
                "Загрузить базу данных", "Database files (*.db)")
        except FileNotFoundError:
            return
        self.db = sqlite3.connect(path)
        self.load_genres()
        self.update_table()

    def execute_query_fetchone(self, query):
        return self.cur.execute(query).fetchone()

    def fill_table(self, header, data):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setHorizontalHeaderLabels(header)
        for row_num, row_data in enumerate(data):
            self.tableWidget.setRowCount(self.tableWidget.rowCount() + 1)
            for el_num, el in enumerate(row_data):
                item = QTableWidgetItem(str(el))
                self.tableWidget.setItem(row_num, el_num, item)
        self.tableWidget.resizeColumnsToContents()

    def set_max_year(self):
        year = datetime.date.today().year
        self.spinBox_year.setMaximum(year)

    def update_table(self):
        self.create_cursor()
        data = self.execute_query("SELECT * FROM Films")
        header = self.get_col_names()
        self.fill_table(header, data)
        self.close_cursor()

    def commit_changes(self):
        self.db.commit()

    def add_film(self):
        if self.db is None:
            self.show_messagebox("Сначала откройте БД!")
            return
        title = self.lineEdit_title.text()
        if not title:
            self.show_messagebox("Введите название!")
            return
        year = self.spinBox_year.value()
        genre = self.comboBox_genre.currentText()
        self.create_cursor()
        genre = self.execute_query_fetchone(
            f"SELECT id FROM genres WHERE title = '{genre}'")[0]
        duration = self.lineEdit_duration.text()
        if duration and (not duration.isnumeric() or int(duration) <= 0):
            self.show_messagebox("Неверная продолжительность!")
            return
        if self.is_in_db(title, year, genre, duration):
            self.show_messagebox("Такой фильм уже есть в БД!")
            return
        self.create_cursor()
        self.execute_query(
            f"INSERT INTO Films(title, year, genre, duration) VALUES ('{title}', '{year}', '{genre}', '{duration}')")
        self.close_cursor()
        self.commit_changes()
        self.update_table()

    def is_in_db(self, title, year, genre, duration):
        self.create_cursor()
        result = self.execute_query_fetchone(f"""SELECT * FROM Films
        WHERE title = '{title}' AND year = {year} AND duration = '{duration}' AND genre = (
        SELECT id FROM genres
        WHERE title = '{genre}'
        )""")
        self.close_cursor()
        return bool(result)

    def show_messagebox(self, message):
        msgbox = QMessageBox(self)
        msgbox.setText(message)
        msgbox.show()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
