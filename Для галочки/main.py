class Point:
    def __init__(self, name, x, y):
        self._name = name
        self._coords = (x, y)

    def get_x(self):
        return self._coords[0]

    def get_y(self):
        return self._coords[1]

    def get_coords(self):
        return self._coords

    def get_name(self):
        return self._name

    def __str__(self):
        return f"{self._name}{self._coords}"

    def __invert__(self):
        return Point(self._name, *reversed(self._coords))

    def __repr__(self):
        return f"Point{(self._name, *self._coords)}"

    def __eq__(self, other):
        return self._coords == other._coords

    def __ne__(self, other):
        return self._coords != other._coords


class CheckMark:
    def __init__(self, point1, point2, point3):
        self._point = (point1, point2, point3)

    def __str__(self):
        return "".join(map(lambda el: el.get_name(), self._point))

    def __bool__(self):
        return bool(self._point[0].get_x() * (self._point[1].get_y() - self._point[2].get_y()) +
                    self._point[1].get_x() * (self._point[2].get_y() - self._point[0].get_y()) +
                    self._point[2].get_x() * (self._point[0].get_y() - self._point[1].get_y())) and \
            self._point[0] != self._point[1] and \
            self._point[1] != self._point[2] and \
            self._point[0] != self._point[2]

    def __eq__(self, other):
        return self._point[1] == other._point[1] and \
            (self._point[0] == other._point[0] or self._point[0] == other._point[2]) and \
            (self._point[2] == other._point[2] or self._point[2] == other._point[0])
