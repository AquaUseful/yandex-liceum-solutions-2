goods = dict(map(lambda words: (words[0], int(
    words[-1])), (input().split("\t") for _ in range(int(input())))))
input()
orders = [0]
curr_order = 0
while True:
    line = input()
    if line == ".":
        break
    elif not line:
        orders.append(0)
        curr_order += 1
        continue
    order = line.split("\t")
    orders[curr_order] += goods[order[0]] * int(order[1])
print(*map(lambda el: f"{el[0] + 1}) {el[1]}",
           enumerate(filter(lambda sum: sum > 0, orders))), sep="\n")
print(f"Итого: {sum(orders)}")
