import unittest
from yandex_testing_lesson import reverse


class TestReverse(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(reverse(""), "")

    def test_one_char(self):
        self.assertEqual(reverse("a"), "a")
        self.assertEqual(reverse("1"), "1")
        self.assertEqual(reverse(" "), " ")

    def test_palyndrome(self):
        self.assertEqual(reverse("aba"), "aba")
        self.assertEqual(reverse("1T34f6f43T1"), "1T34f6f43T1")
        self.assertEqual(reverse(".!.::;::.!."), ".!.::;::.!.")

    def test_normal(self):
        self.assertEqual(reverse("abc"), "cba")
        self.assertEqual(reverse("erIUGEWf"), "fWEGUIre")
        self.assertEqual(reverse("54367"), "76345")

    def test_noiterable(self):
        with self.assertRaises(TypeError):
            reverse(2345)
        with self.assertRaises(TypeError):
            reverse(345.456)
        with self.assertRaises(TypeError):
            reverse(None)
        with self.assertRaises(TypeError):
            reverse(print)

    def test_iterable_nostring(self):
        with self.assertRaises(TypeError):
            reverse([])
        with self.assertRaises(TypeError):
            reverse((1, 4, 5))
        with self.assertRaises(TypeError):
            reverse({})
        with self.assertRaises(TypeError):
            reverse(["345", "abcfd", 56, None])
        with self.assertRaises(TypeError):
            reverse(map(lambda a: a ** 2), range(100))


if __name__ == "__main__":
    unittest.main()
