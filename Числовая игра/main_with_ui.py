from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from random import randint
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(98, 170)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lcdNumber = QtWidgets.QLCDNumber(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.lcdNumber.sizePolicy().hasHeightForWidth())
        self.lcdNumber.setSizePolicy(sizePolicy)
        self.lcdNumber.setMinimumSize(QtCore.QSize(0, 40))
        self.lcdNumber.setObjectName("lcdNumber")
        self.verticalLayout.addWidget(self.lcdNumber)
        self.buttonPlus = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.buttonPlus.sizePolicy().hasHeightForWidth())
        self.buttonPlus.setSizePolicy(sizePolicy)
        self.buttonPlus.setObjectName("buttonPlus")
        self.verticalLayout.addWidget(self.buttonPlus)
        self.buttonMinus = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.buttonMinus.sizePolicy().hasHeightForWidth())
        self.buttonMinus.setSizePolicy(sizePolicy)
        self.buttonMinus.setMinimumSize(QtCore.QSize(0, 0))
        self.buttonMinus.setObjectName("buttonMinus")
        self.verticalLayout.addWidget(self.buttonMinus)
        self.buttonReset = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.buttonReset.sizePolicy().hasHeightForWidth())
        self.buttonReset.setSizePolicy(sizePolicy)
        self.buttonReset.setObjectName("buttonReset")
        self.verticalLayout.addWidget(self.buttonReset)
        self.labelMovesLeft = QtWidgets.QLabel(self.centralwidget)
        self.labelMovesLeft.setObjectName("labelMovesLeft")
        self.verticalLayout.addWidget(self.labelMovesLeft)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Числовая игра"))
        self.buttonPlus.setText(_translate("MainWindow", "PushButton"))
        self.buttonMinus.setText(_translate("MainWindow", "PushButton"))
        self.buttonReset.setText(_translate("MainWindow", "Заново"))
        self.labelMovesLeft.setText(_translate("MainWindow", "TextLabel"))


class NumberGame(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.init_game()

    def init_game(self):
        self._num = randint(1, 10)
        self._plus = randint(1, 10)
        self._minus = randint(1, 10)
        self._moves = 10
        self.update_labels()
        self.update_buttons()

    def initUI(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.buttonMinus.clicked.connect(self.num_substract)
        self.buttonPlus.clicked.connect(self.num_add)
        self.buttonReset.clicked.connect(self.init_game)

    def update_buttons(self):
        self.buttonMinus.setText(f"-{self._minus}")
        self.buttonPlus.setText(f"+{self._plus}")

    def make_move(self):
        self._moves -= 1
        self.update_labels()
        self.check_win_lose()

    def update_labels(self):
        self.labelMovesLeft.setText(f"Ост. ходов: {self._moves}")
        self.lcdNumber.display(self._num)

    def num_add(self):
        self._num += self._plus
        self.make_move()

    def num_substract(self):
        self._num -= self._minus
        self.make_move()

    def check_win_lose(self):
        if self._num == 0:
            msg = QMessageBox(self)
            msg.setText("Вы выиграли!")
        elif self._moves == 0:
            msg = QMessageBox(self)
            msg.setText("Вы проиграли!")
        else:
            return
        msg.show()
        self.init_game()


def main():
    app = QApplication(argv)
    game = NumberGame()
    game.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
