# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/catsatan/Документы/yandex-liceum-2/Числовая игра/MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(98, 170)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lcdNumber = QtWidgets.QLCDNumber(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lcdNumber.sizePolicy().hasHeightForWidth())
        self.lcdNumber.setSizePolicy(sizePolicy)
        self.lcdNumber.setMinimumSize(QtCore.QSize(0, 40))
        self.lcdNumber.setObjectName("lcdNumber")
        self.verticalLayout.addWidget(self.lcdNumber)
        self.buttonPlus = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonPlus.sizePolicy().hasHeightForWidth())
        self.buttonPlus.setSizePolicy(sizePolicy)
        self.buttonPlus.setObjectName("buttonPlus")
        self.verticalLayout.addWidget(self.buttonPlus)
        self.buttonMinus = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonMinus.sizePolicy().hasHeightForWidth())
        self.buttonMinus.setSizePolicy(sizePolicy)
        self.buttonMinus.setMinimumSize(QtCore.QSize(0, 0))
        self.buttonMinus.setObjectName("buttonMinus")
        self.verticalLayout.addWidget(self.buttonMinus)
        self.buttonReset = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonReset.sizePolicy().hasHeightForWidth())
        self.buttonReset.setSizePolicy(sizePolicy)
        self.buttonReset.setObjectName("buttonReset")
        self.verticalLayout.addWidget(self.buttonReset)
        self.labelMovesLeft = QtWidgets.QLabel(self.centralwidget)
        self.labelMovesLeft.setObjectName("labelMovesLeft")
        self.verticalLayout.addWidget(self.labelMovesLeft)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Числовая игра"))
        self.buttonPlus.setText(_translate("MainWindow", "PushButton"))
        self.buttonMinus.setText(_translate("MainWindow", "PushButton"))
        self.buttonReset.setText(_translate("MainWindow", "Заново"))
        self.labelMovesLeft.setText(_translate("MainWindow", "TextLabel"))
