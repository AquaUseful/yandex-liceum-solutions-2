from sys import argv, exit
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from random import randrange


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.setMouseTracking(True)

    def initUi(self):
        self.setGeometry(300, 300, 500, 500)
        self.button = QPushButton(self)
        self.button.setText("Тык!")
        self.button.resize(50, 50)
        self.button.move(225, 225)
        self.button.clicked.connect(exit)

    def mouseMoveEvent(self, event):
        bbox_x_min, bbox_x_max = self.button.x() - 20, self.button.x() + 70
        bbox_y_min, bbox_y_max = self.button.y() - 20, self.button.y() + 70
        max_x, max_y = self.width() - 50, self.height() - 50
        if bbox_x_min <= event.x() <= bbox_x_max and bbox_y_min <= event.y() <= bbox_y_max:
            self.button.move(randrange(0, max_x), randrange(0, max_y))


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
