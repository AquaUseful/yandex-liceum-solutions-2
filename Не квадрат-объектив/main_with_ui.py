from sys import argv, exit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QColorDialog, QFileDialog
from PyQt5.QtGui import QPixmap
from PIL import Image, ImageQt
from PIL.ImageDraw import Draw
from math import sin, cos, pi


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(728, 532)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.checkBox_reverse_ratio = QtWidgets.QCheckBox(self.groupBox)
        self.checkBox_reverse_ratio.setObjectName("checkBox_reverse_ratio")
        self.gridLayout_2.addWidget(self.checkBox_reverse_ratio, 6, 1, 1, 1)
        self.spinBox_iters = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_iters.setMinimum(1)
        self.spinBox_iters.setMaximum(950)
        self.spinBox_iters.setObjectName("spinBox_iters")
        self.gridLayout_2.addWidget(self.spinBox_iters, 3, 1, 1, 1)
        self.doubleSpinBox_ratio = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_ratio.setDecimals(3)
        self.doubleSpinBox_ratio.setMinimum(0.0)
        self.doubleSpinBox_ratio.setMaximum(1.0)
        self.doubleSpinBox_ratio.setSingleStep(0.01)
        self.doubleSpinBox_ratio.setProperty("value", 1.0)
        self.doubleSpinBox_ratio.setObjectName("doubleSpinBox_ratio")
        self.gridLayout_2.addWidget(self.doubleSpinBox_ratio, 5, 1, 1, 1)
        self.spinBox_img_size = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_img_size.setMinimum(10)
        self.spinBox_img_size.setMaximum(1000)
        self.spinBox_img_size.setSingleStep(10)
        self.spinBox_img_size.setObjectName("spinBox_img_size")
        self.gridLayout_2.addWidget(self.spinBox_img_size, 0, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 5, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.groupBox)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.groupBox)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 2, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 3, 0, 1, 1)
        self.spinBox_side_length = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_side_length.setMinimum(1)
        self.spinBox_side_length.setMaximum(500)
        self.spinBox_side_length.setSingleStep(5)
        self.spinBox_side_length.setObjectName("spinBox_side_length")
        self.gridLayout_2.addWidget(self.spinBox_side_length, 1, 1, 1, 1)
        self.spinBox_sides = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_sides.setMinimum(3)
        self.spinBox_sides.setMaximum(100)
        self.spinBox_sides.setObjectName("spinBox_sides")
        self.gridLayout_2.addWidget(self.spinBox_sides, 2, 1, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.groupBox)
        self.label_7.setObjectName("label_7")
        self.gridLayout_2.addWidget(self.label_7, 4, 0, 1, 1)
        self.spinBox_rotation = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_rotation.setMaximum(360)
        self.spinBox_rotation.setObjectName("spinBox_rotation")
        self.gridLayout_2.addWidget(self.spinBox_rotation, 4, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_ch_line_color = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_ch_line_color.setObjectName("pushButton_ch_line_color")
        self.horizontalLayout.addWidget(self.pushButton_ch_line_color)
        self.pushButton_ch_bg_color = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_ch_bg_color.setObjectName("pushButton_ch_bg_color")
        self.horizontalLayout.addWidget(self.pushButton_ch_bg_color)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.pushButton_update = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_update.setObjectName("pushButton_update")
        self.verticalLayout.addWidget(self.pushButton_update)
        self.pushButton_clear = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_clear.setObjectName("pushButton_clear")
        self.verticalLayout.addWidget(self.pushButton_clear)
        self.pushButton_save = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_save.setObjectName("pushButton_save")
        self.verticalLayout.addWidget(self.pushButton_save)
        self.horizontalLayout_2.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.horizontalLayout_2.addWidget(self.groupBox_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Не квадрат объектив"))
        self.groupBox.setTitle(_translate("MainWindow", "Параметры"))
        self.checkBox_reverse_ratio.setText(
            _translate("MainWindow", "Обратить коэффициент"))
        self.label_3.setText(_translate(
            "MainWindow", "Коэффициент масштабирования"))
        self.label_5.setText(_translate(
            "MainWindow", "Размер изображения в пикселях"))
        self.label_2.setText(_translate(
            "MainWindow", "Длина стороны в пикселях"))
        self.label_6.setText(_translate("MainWindow", "Количество сторон"))
        self.label_4.setText(_translate("MainWindow", "Количество повторений"))
        self.label_7.setText(_translate("MainWindow", "Угол поворота фигуры"))
        self.pushButton_ch_line_color.setText(
            _translate("MainWindow", "Выбрать цвет линий"))
        self.pushButton_ch_bg_color.setText(
            _translate("MainWindow", "Выбрать цвет фона"))
        self.pushButton_update.setText(_translate("MainWindow", "Обновить"))
        self.pushButton_clear.setText(_translate("MainWindow", "Очистить"))
        self.pushButton_save.setText(
            _translate("MainWindow", "Сохранить в файл"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Результат"))
        self.label.setText(_translate("MainWindow", "TextLabel"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.bg_color = "#000000"
        self.line_color = "#FFFFFF"
        self.update_label()

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.pushButton_update.clicked.connect(self.update_label)
        self.pushButton_clear.clicked.connect(self.clear_label)
        self.pushButton_ch_bg_color.clicked.connect(self.request_bg_color)
        self.pushButton_ch_line_color.clicked.connect(self.request_line_color)
        self.pushButton_save.clicked.connect(self.save_image)
        self.spinBox_img_size.valueChanged.connect(self.update_label)
        self.spinBox_iters.valueChanged.connect(self.update_label)
        self.spinBox_side_length.valueChanged.connect(self.update_label)
        self.spinBox_sides.valueChanged.connect(self.update_label)
        self.doubleSpinBox_ratio.valueChanged.connect(self.update_label)
        self.spinBox_rotation.valueChanged.connect(self.update_label)
        self.checkBox_reverse_ratio.clicked.connect(self.update_label)

    def request_open_path(self, request, filter):
        path = QFileDialog.getOpenFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def requset_save_path(self, request, filter):
        path = QFileDialog.getSaveFileName(
            self, request, "", filter)[0]
        if path:
            return path
        raise FileNotFoundError

    def request_color(self):
        color = QColorDialog.getColor()
        if color.isValid():
            return color.name()
        else:
            raise ValueError

    @staticmethod
    def image_to_pixmap(image):
        return QPixmap.fromImage(ImageQt.ImageQt(image))

    def request_bg_color(self):
        try:
            self.bg_color = self.request_color()
        except ValueError:
            return

    def request_line_color(self):
        try:
            self.line_color = self.request_color()
        except ValueError:
            return

    def generate_magic_image(self):
        size = self.spinBox_img_size.value()
        side_length = self.spinBox_side_length.value()
        sides = self.spinBox_sides.value()
        ratio = self.doubleSpinBox_ratio.value()
        if self.checkBox_reverse_ratio.isChecked() and ratio > 0:
            ratio = 1 / ratio
        iters = self.spinBox_iters.value()
        rotation = deg_to_rad(self.spinBox_rotation.value())
        self.image = gen_image_wrapped(size, side_length, sides, ratio, iters, rotation,
                                       self.bg_color, self.line_color)

    def save_image(self):
        try:
            path = self.requset_save_path("Save image", "All files")
        except FileNotFoundError:
            return
        image = self.image.convert("RGB")
        image.save(path)

    def update_label(self):
        self.generate_magic_image()
        pixmap = self.image_to_pixmap(self.image)
        self.clear_label()
        self.label.setPixmap(pixmap)

    def clear_label(self):
        self.label.clear()


def gen_coords(radius, sides, shift, rotation):
    return tuple((int(cos(2 * pi * k / sides + rotation) * radius + shift[0]),
                  int(sin(2 * pi * k / sides + rotation) * radius + shift[1])) for k in range(sides))


def gen_image(size, coords, ratio, iters, bg_color, line_color):
    img = Image.new("RGBA", (size, ) * 2, bg_color)
    dr = Draw(img)
    draw_magic(dr, coords, ratio, iters, line_color)
    del dr
    return img


def draw_magic(draw_obj, coords, ratio, iters, line_color):
    draw_obj.polygon(coords, outline=line_color)
    if iters == 1:
        return
    new_coords = map(lambda n: find_part_coords(
        coords[n], coords[n - 1], ratio), range(len(coords)))
    draw_magic(draw_obj, tuple(new_coords), ratio, iters - 1, line_color)


def find_part_coords(coords1, coords2, ratio):
    return ((round(coords1[0] + ratio * coords2[0]) / (1 + ratio)),
            (round(coords1[1] + ratio * coords2[1]) / (1 + ratio)))


def radius_around_polygon(side_length, sides):
    return round(0.5 * side_length / sin(pi / sides))


def gen_image_wrapped(image_size, side_length, sides, ratio, iters, rotation, bg_color, line_color):
    radius = radius_around_polygon(side_length, sides)
    coords = gen_coords(radius, sides, (image_size // 2,) * 2, rotation)
    return gen_image(image_size, coords, ratio, iters, bg_color, line_color)


def deg_to_rad(angle):
    return angle * pi / 180


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
