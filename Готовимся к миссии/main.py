import flask

app = flask.Flask(__name__)

@app.route("/<pagename>")
@app.route("/index/<pagename>")
def index(pagename):
    return flask.render_template("base.htm", title=pagename)

if __name__ == "__main__":
    app.run(host="::1", port=8080, debug=True)