from sys import stdin
sample = ("далек", "далека", "далеку", "далека", "далеком", "далеке",
          "далеки", "далеков", "далекам", "далеками", "далеках")
print(len(tuple(filter(lambda line: any(
    map(lambda word: word in sample, line.lower().split())), stdin.readlines()))))
