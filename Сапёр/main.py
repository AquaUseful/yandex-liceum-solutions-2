from random import randrange

RESET_ALL = "\33[0m"
BOLD = "\33[1m"
BLINK = "\33[5m"
REVERSE = "\33[7m"
RED = "\33[31m"
BLUE = "\33[34m"
LIGHT_RED = "\33[91m"
LIGHT_GREEN = "\33[92m"
LIGHT_YELLOW = "\33[93m"


def gen_field(size, mines, start_cell):
    field = gen_dummy_field(size)
    mine_num = 0
    while mine_num != mines:
        random = (randrange(0, size[0]), randrange(0, size[1]))
        if field[random[0]][random[1]][1] or random == start_cell:
            continue
        field[random[0]][random[1]][1] = True
        mine_num += 1
    return field


def gen_dummy_field(size):
    return [[[-1, False, ""] for _ in range(size[1])] for _ in range(size[0])]


def print_field(field, sel_cell_coords):
    print(*map(lambda line_num, line:
               " ".join(map(lambda el_num, el: make_selected(el[2])
                            if (line_num, el_num) == tuple(sel_cell_coords) else el[2],
                            range(len(line)), line)), range(len(field)), field), sep='\n')


def make_selected(cell_sign):
    if cell_sign == "-":
        return f"{LIGHT_GREEN}{BOLD}+{RESET_ALL}"
    else:
        return f"{LIGHT_YELLOW}{REVERSE}{BOLD}{cell_sign}{RESET_ALL}"


def put_signs(field, put_mines=False):
    for cell in sum(field, []):
        if put_mines and cell[1]:
            cell[2] = f"{LIGHT_RED}{BLINK}*{RESET_ALL}"
        elif is_question(cell) or is_flag(cell):
            continue
        elif cell[0] in (-1, 0):
            cell[2] = {-1: "-", 0: "#"}[cell[0]]
        else:
            cell[2] = str(cell[0])


def open_cell(field, cell_coords):
    cell = get_cell(field, (cell_coords[0], cell_coords[1]))
    mines = neighbor_mines(field, cell_coords)
    free = neighbor_free(field, cell_coords)
    if not cell[1]:
        cell[0] = len(mines)
    if cell[0] == 0:
        for fr in free:
            open_cell(field, fr)


def neighbor_mines(field, cell_coords):
    field_size = (len(field), len(field[0]))
    mines = []
    for row_shift in (-1, 0, 1):
        for col_shift in (-1, 0, 1):
            if row_shift == col_shift == 0 or \
                    cell_coords[0] + row_shift >= field_size[0] or \
                    cell_coords[1] + col_shift >= field_size[1] or \
                    cell_coords[0] + row_shift < 0 or \
                    cell_coords[1] + col_shift < 0:
                continue
            cell = field[cell_coords[0] +
                         row_shift][cell_coords[1] + col_shift]
            if is_mine(cell):
                mines.append(
                    (cell_coords[0] + row_shift, cell_coords[1] + col_shift))
    return mines


def neighbor_free(field, cell_coords):
    field_size = (len(field), len(field[0]))
    free = []
    for row_shift in (-1, 0, 1):
        for col_shift in (-1, 0, 1):
            if row_shift == col_shift == 0 or \
                    cell_coords[0] + row_shift >= field_size[0] or \
                    cell_coords[1] + col_shift >= field_size[1] or \
                    cell_coords[0] + row_shift < 0 or \
                    cell_coords[1] + col_shift < 0:
                continue
            cell = field[cell_coords[0] +
                         row_shift][cell_coords[1] + col_shift]
            if not is_mine(cell) and is_openable(cell):
                free.append((cell_coords[0] + row_shift,
                             cell_coords[1] + col_shift))
    return free


def get_cell(field, cell_coords):
    return field[cell_coords[0]][cell_coords[1]]


def is_mine(cell):
    return cell[1]


def is_opened(cell):
    return cell[0] != -1


def is_openable(cell):
    return cell[0] == -1 and not (is_flag(cell) or is_question(cell))


def check_win(field):
    return all(map(lambda cell: cell[0] != -1 or cell[1], sum(field, [])))


def put_question(cell):
    cell[2] = f"{BLUE}?{RESET_ALL}"


def put_flag(cell):
    cell[2] = f"{RED}F{RESET_ALL}"


def clear_sign(cell):
    cell[2] = ""


def is_flag(cell):
    return "F" in cell[2]


def is_question(cell):
    return "?" in cell[2]


def int_input(promt):
    while True:
        val = input(promt)
        if val.isnumeric():
            return int(val)
        print(f"{LIGHT_RED}{BOLD}Введено неверное значение!{RESET_ALL}")


class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""

    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self):
        return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty
        import sys

    def __call__(self):
        import sys
        import tty
        import termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


def main():
    getch = _Getch()
    new_game = True
    while True:
        lose = False
        win = False
        if new_game:
            new_game = False
            choice = ""
            # Difficulty levels are stolen from GNOME Mines
            print(f"{BOLD}Выберите сложность игры:{RESET_ALL}")
            print("1 Легко: 8x8, 10 мин")
            print("2 Средне: 16x16, 40 мин")
            print("3 Сложно: 16x30, 99 мин")
            print("4 Пользовательская сложность")
            print("0 Выйти")
            choice = int_input("> ")
            if choice not in (0, 1, 2, 3, 4):
                print(f"{LIGHT_RED}{BOLD}Введено неверное значение!{RESET_ALL}")
                continue
            elif choice == 0:
                break
            elif choice == 4:
                print(f"{BOLD}Выберите размер поля:{RESET_ALL}")
                while True:
                    field_size = (int_input("Количество строк: "),
                                  int_input("Количество колонок: "))
                    for dens in range(100):
                        if round(field_size[0] * field_size[1] / 100 * dens) > 0:
                            min_density = dens
                            break
                    else:
                        print(
                            f"{LIGHT_RED}{BOLD}Введено неверное значение!{RESET_ALL}")
                        continue
                    break
                while True:
                    density = int_input(
                        f"Процент заполнения минами ({min_density}-99): ")
                    if min_density <= density < 100:
                        break
                    print(f"{LIGHT_RED}{BOLD}Введено неверное значение!{RESET_ALL}")
                mines = round(field_size[0] * field_size[1] / 100 * density)
            else:
                field_size = {1: (8, 8), 2: (16, 16), 3: (16, 30)}[choice]
                mines = {1: 10, 2: 40, 3: 99}[choice]
        first_move = True
        field = gen_dummy_field(field_size)
        sel_cell_coords = [field_size[0] // 2, field_size[1] // 2]
        flags = mines
        # Using WASD because arrow keys have 3 byte long escape sequences and
        # could not be read by getch()
        print(f"{BOLD}Управление:{RESET_ALL}")
        print("W, A, S, D - двигать курсор")
        print("Пробел - открыть клетку")
        print("F - поставить/снять флажок")
        print("Q - поставить/снять знак вопроса")
        print("R - начать игру заново")
        print("X - выйти в меню")
        while True:
            sel_cell = field[sel_cell_coords[0]][sel_cell_coords[1]]
            put_signs(field)
            print_field(field, sel_cell_coords)
            print(f"Поставлено флагов: {mines}/{mines - flags}")
            while True:
                char = getch().lower()
                if char in ("w", "a", "s", "d", "f", "q", "r", "x", " "):
                    break
            if char == "r":
                break
            elif char == "x":
                new_game = True
                break
            elif char == " ":
                if is_openable(sel_cell):
                    if first_move:
                        field = gen_field(field_size, mines, sel_cell_coords)
                        first_move = False
                    if is_mine(sel_cell):
                        lose = True
                        break
                    else:
                        open_cell(field, sel_cell_coords)
            elif char == "f":
                if is_flag(sel_cell):
                    clear_sign(sel_cell)
                    flags += 1
                elif is_openable(sel_cell) and flags > 0:
                    put_flag(sel_cell)
                    flags -= 1

            elif char == "q":
                if is_question(sel_cell):
                    clear_sign(sel_cell)
                elif is_openable(sel_cell):
                    put_question(sel_cell)
            elif char == "w":
                sel_cell_coords[0] = (sel_cell_coords[0] - 1) % field_size[0]
            elif char == "s":
                sel_cell_coords[0] = (sel_cell_coords[0] + 1) % field_size[0]
            elif char == "a":
                sel_cell_coords[1] = (sel_cell_coords[1] - 1) % field_size[1]
            elif char == "d":
                sel_cell_coords[1] = (sel_cell_coords[1] + 1) % field_size[1]
            if check_win(field):
                win = True
                break
            print("\n")
        if not win and not lose:
            continue
        if win:
            print(f"{LIGHT_GREEN}{BOLD}ВЫ ВЫИГРАЛИ!{RESET_ALL}")
        elif lose:
            print(f"{LIGHT_RED}{BOLD}ВЫ ПРОИГРАЛИ!{RESET_ALL}")
        put_signs(field, True)
        print_field(field, sel_cell_coords)
        print(f"{BOLD}Начать заново или выйти?{RESET_ALL}")
        print("1 Заново")
        print("2 Выйти в меню")
        print("0 Выйти из игры")
        while True:
            choice = int_input("> ")
            if choice in (0, 1, 2):
                break
        if choice == 0:
            break
        elif choice == 2:
            new_game = True


if __name__ == "__main__":
    main()
