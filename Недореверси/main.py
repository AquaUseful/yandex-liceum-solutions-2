import pygame
from random import randint


class Board(object):
    def __init__(self, width: int, height: int, fillval: int = 0) -> None:
        self.width = width
        self.height = height
        self.board = [[randint(0, 1) for _ in range(height)]
                      for _ in range(width)]
        self.set_view(10, 10, 50)

    def set_view(self, left: int, top: int, cell_size: int) -> None:
        self.left = left
        self.top = top
        self.cell_size = cell_size

    def get_pixel_size(self) -> tuple:
        return (self.cell_size * self.width, self.cell_size * self.height)

    def get_pixel_width(self) -> int:
        return self.cell_size * self.width

    def get_pixel_height(self) -> int:
        return self.cell_size * self.height

    def render(self, border=True) -> None:
        border_color = pygame.Color("white")
        for x in range(self.width):
            for y in range(self.height):
                fill_color = pygame.Color(
                    {0: "black", 1: "white"}[self.board[x][y]])
                rect = pygame.Rect((x * self.cell_size + self.left, y *
                                    self.cell_size + self.top), (self.cell_size, ) * 2)
                pygame.draw.ellipse(screen, fill_color, rect, 0)
                if border:
                    pygame.draw.rect(screen, border_color, rect, 1)

    def relative_pos(self, pos: tuple) -> tuple:
        return (pos[0] - self.left, pos[1] - self.top)\


    def get_click(self, pos: tuple) -> None:
        cell = self.get_cell(pos)
        print(cell)
        self.on_click(cell)

    def on_click(self, cell: tuple) -> None:
        if cell is None:
            return
        val = self.board[cell[0]][cell[1]]
        for x in range(self.width):
            self.board[x][cell[1]] = val
        for y in range(self.height):
            self.board[cell[0]][y] = val

    def get_cell(self, pos: tuple) -> tuple:
        if all(map(lambda pos, maximum: 0 < pos < maximum, self.relative_pos(pos), self.get_pixel_size())):
            return tuple(coord // self.cell_size for coord in self.relative_pos(pos))
        else:
            return None


running = True
fps = 100

size = int(input("Input field size: "))
board = Board(size, size)
screen_height, screen_width = size * 50 + 20, size * 50 + 20

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            board.get_click(event.pos)
    screen.fill(pygame.Color("orange"))
    board.render(border=False)
    pygame.display.flip()
    clock.tick(fps)
pygame.quit()
