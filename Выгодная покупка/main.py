import csv
with open("wares.csv") as csvfile:
    rd = csv.reader(csvfile, delimiter=';', quotechar='"')
    goods = sorted(tuple(map(lambda line: (line[0], int(
        line[1])), rd)), key=lambda el: el[1])
money = 1000
buy = []
for prod in goods:
    for i in range(10):
        money -= prod[1]
        if money >= 0:
            buy.append(prod[0])
            continue
        break
    else:
        continue
    break
if buy:
    print(*buy, sep=', ')
else:
    print("error")
