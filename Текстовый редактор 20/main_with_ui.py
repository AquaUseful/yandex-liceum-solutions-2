from sys import argv, exit
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QMessageBox


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setContentsMargins(10, 10, 10, 10)
        self.gridLayout.setObjectName("gridLayout")
        self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser.setReadOnly(False)
        self.textBrowser.setObjectName("textBrowser")
        self.gridLayout.addWidget(self.textBrowser, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menuBar.setObjectName("menuBar")
        self.menu = QtWidgets.QMenu(self.menuBar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menuBar)
        self.action_3 = QtWidgets.QAction(MainWindow)
        self.action_3.setObjectName("action_3")
        self.action_4 = QtWidgets.QAction(MainWindow)
        self.action_4.setObjectName("action_4")
        self.action_6 = QtWidgets.QAction(MainWindow)
        self.action_6.setObjectName("action_6")
        self.action_7 = QtWidgets.QAction(MainWindow)
        self.action_7.setObjectName("action_7")
        self.action_new = QtWidgets.QAction(MainWindow)
        self.action_new.setObjectName("action_new")
        self.action_open = QtWidgets.QAction(MainWindow)
        self.action_open.setObjectName("action_open")
        self.action_save = QtWidgets.QAction(MainWindow)
        self.action_save.setObjectName("action_save")
        self.action_saveas = QtWidgets.QAction(MainWindow)
        self.action_saveas.setObjectName("action_saveas")
        self.action_close = QtWidgets.QAction(MainWindow)
        self.action_close.setObjectName("action_close")
        self.menu.addAction(self.action_new)
        self.menu.addAction(self.action_open)
        self.menu.addSeparator()
        self.menu.addAction(self.action_save)
        self.menu.addAction(self.action_saveas)
        self.menu.addSeparator()
        self.menu.addAction(self.action_close)
        self.menuBar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Текстовый редактор 2.0"))
        self.menu.setTitle(_translate("MainWindow", "Файл"))
        self.action_3.setText(_translate("MainWindow", "Сохранить"))
        self.action_4.setText(_translate("MainWindow", "Сохранить как..."))
        self.action_6.setText(_translate("MainWindow", "Выход"))
        self.action_7.setText(_translate("MainWindow", "Создвть"))
        self.action_new.setText(_translate("MainWindow", "Создать"))
        self.action_open.setText(_translate("MainWindow", "Открыть..."))
        self.action_save.setText(_translate("MainWindow", "Сохранить"))
        self.action_saveas.setText(_translate(
            "MainWindow", "Сохранить как..."))
        self.action_close.setText(_translate("MainWindow", "Выход"))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.initUi()
        self.path = ""

    def initUi(self):
        self.setupUi(self)
        self.retranslateUi(self)
        self.action_new.triggered.connect(self.ui_new)
        self.action_open.triggered.connect(self.ui_open)
        self.action_save.triggered.connect(self.ui_save)
        self.action_saveas.triggered.connect(self.ui_saveas)
        self.action_close.triggered.connect(self.ui_close)

    def request_open_path(self, request):
        path = QFileDialog.getOpenFileName(
            self, request, "", "All files (*.*)")[0]
        if path:
            return path
        raise FileNotFoundError

    def requset_save_path(self, request):
        path = QFileDialog.getSaveFileName(
            self, request, "", "All files (*.*)")[0]
        if path:
            return path
        raise FileNotFoundError

    @staticmethod
    def load_file(path):
        with open(path, "r") as f:
            data = f.read()
        return data

    @staticmethod
    def save_file(path, data):
        with open(path, "w") as f:
            f.write(data)

    def show_messagebox(self, message):
        msgbox = QMessageBox()
        msgbox.setText(message)
        msgbox.show()

    def ui_request_path(self, request, saving=False):
        try:
            if saving:
                self.path = self.requset_save_path(request)
            else:
                self.path = self.request_open_path(request)
        except FileNotFoundError:
            self.show_messagebox("Выберите файл")

    def ui_open(self, request):
        self.ui_request_path("Выберите файл")
        self.textBrowser.setPlainText(self.load_file(self.path))

    def ui_save(self):
        if self.path:
            self.save_file(self.path, self.textBrowser.toPlainText())
        else:
            self.ui_saveas()

    def ui_saveas(self):
        self.ui_request_path("Сохранить", True)
        if self.path:
            self.ui_save()

    def ui_new(self):
        if self.path:
            self.ui_save
        self.path = ""
        self.textBrowser.setPlainText("")

    def ui_close(self):
        exit()


def main():
    app = QApplication(argv)
    window = MainWindow()
    window.show()
    exit(app.exec_())


if __name__ == "__main__":
    main()
