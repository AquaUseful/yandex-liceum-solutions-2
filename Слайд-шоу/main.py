import os
import sys
import requests
import pygame
import itertools

server_address = "https://static-maps.yandex.ru/1.x/"
slide_params = (("56.7822,54.4196", "0.002,0.002"),
                ("-2.355511,57.337241", "0.001,0.001"),
                ("127.783714,26.357851", "0.001,0.001"))
slide_count = len(slide_params)
screen_size = (600, 450)
for n, (ll, spn) in enumerate(slide_params):
    params = {
        "l": "sat",
        "spn": spn,
        "ll": ll
    }
    response = requests.get(server_address, params=params)
    if not response:
        print("HTTP status:", response.status_code, "(", response.reason, ")")
        sys.exit(1)
    slide_name = f"slide{n}.png"
    with open(slide_name, "wb") as f:
        f.write(response.content)
pygame.init()
screen = pygame.display.set_mode(screen_size)
clock = pygame.time.Clock()
running = True
slide_num = itertools.cycle(range(slide_count))
curr_slide = f"slide{next(slide_num)}.png"
screen.blit(pygame.image.load(curr_slide), (0, 0))
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            curr_slide = f"slide{next(slide_num)}.png"
            screen.blit(pygame.image.load(curr_slide), (0, 0))
    pygame.display.flip()
pygame.quit()
for n in range(slide_count):
    slide_name = f"slide{n}.png"
    os.remove(slide_name)
