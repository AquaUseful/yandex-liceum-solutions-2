# Импортируем необходимые классы.
from telegram.ext import Updater, MessageHandler, Filters
from telegram.ext import CallbackContext, CommandHandler

TOKEN = "1097814273:AAH4lUmGXA8wYagzeBcX6RAz87hp90enV4A"
REQUEST_KWARGS = {
    'proxy_url': 'socks5h://127.0.0.1:9050'
}


def echo(update, context):
    update.message.reply_text(f"Я получил сообщение '{update.message.text}'")


def main():
    updater = Updater(token=TOKEN,
                      use_context=True, request_kwargs=REQUEST_KWARGS)

    dp = updater.dispatcher
    text_handler = MessageHandler(Filters.text, echo)

    dp.add_handler(text_handler)

    updater.start_polling(timeout=20)

    updater.idle()


if __name__ == '__main__':
    main()
