from telegram.ext import Updater, MessageHandler, Filters
from telegram.ext import CallbackContext, CommandHandler
import time

TOKEN = ""
REQUEST_KWARGS = {
    'proxy_url': 'socks5h://127.0.0.1:9050'
}


def echo(update, context):
    update.message.reply_text(f"Я получил сообщение '{update.message.text}'")


def cmd_time(update, context):
    tl = time.asctime().split()
    time_str = tl[3]
    update.message.reply_text(time_str)


def cmd_date(update, context):
    tl = time.asctime().split()
    tl.pop(3)
    date_str = " ".join(tl)
    update.message.reply_text(date_str)


def main():
    updater = Updater(token=TOKEN,
                      use_context=True, request_kwargs=REQUEST_KWARGS)

    dp = updater.dispatcher
    text_handler = MessageHandler(Filters.text, echo)
    dp.add_handler(text_handler)
    dp.add_handler(CommandHandler("time", cmd_time))
    dp.add_handler(CommandHandler("date", cmd_date))

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
