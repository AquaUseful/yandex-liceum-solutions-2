from yandex_testing_lesson import strip_punctuation_ru


def testing_function(func):
    test_data = (("", ""),
                 (" ", ""),
                 ("слово", "слово"),
                 ("слово.ещё", "слово ещё"),
                 ("один,два", "один два"),
                 ("кое-что, и кое-кто", "кое-что и кое-кто"),
                 ("Ура, товарищи! Победа коммунизма не за горами!",
                  "Ура товарищи Победа коммунизма не за горами"),
                 ("Тире - особый знак препинания", "Тире особый знак препинания"),
                 ("А знаки вопроса удаляем? Это я у вас спрашиваю!",
                  "А знаки вопроса удаляем Это я у вас спрашиваю"))
    for input_data, correct_data in test_data:
        try:
            returned = func(input_data)
        except Exception as exc:
            break
        else:
            if returned != correct_data:
                break
    else:
        print("YES")
        return
    print("NO")


testing_function(strip_punctuation_ru)
