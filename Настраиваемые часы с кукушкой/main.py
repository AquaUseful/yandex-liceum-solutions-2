import datetime
import schedule
import sys


def job(message, ignore_time):
    hours = datetime.datetime.now().hour
    if ignore_time[0] <= hours <= ignore_time[1]:
        return
    if hours > 12:
        hours -= 12
    elif hours == 0:
        hours = 12
    for _ in range(hours):
        print(message)


params = sys.argv[1:]
if len(params) == 2:
    message = params[0]
    ignore = tuple(map(int, params[1].split("-")))
    schedule.every().hour.at(":00").do(job, message=message, ignore_time=ignore)
    while True:
        schedule.run_pending()
else:
    print(f"Invalid params count! Got {len(params)} when 2 excepted")
