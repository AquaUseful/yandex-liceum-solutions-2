import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--sort", action="store_true")
parser.add_argument("pairs", nargs="+", type=str)
args = parser.parse_args()

pairs = dict()
for pair in args.pairs:
    pair = pair.split("=")
    pairs[pair[0]] = pair[1]
pairs = pairs.items()
print("\n".join(map(lambda val: f"Key: {val[0]}\tValue: {val[1]}",
                    sorted(pairs, key=lambda val: val[0]) if args.sort else pairs)))
